# central

latest release is in maven central

# covered functionality

- Crud applications based on jakarta ee 10, jpa3, jsf and primefaces
  - a jpa module that can be used on its own
  - a primefaces module
- Aspects covered:
  - security
  - transaction demarcation
  - lazy loading into data layer
  - application configuration using CDI ( @Inject ) in a separate dependency
  - logout
  - record level history, including querying
  - dynamic and named jpa queries with support for all comparisons
  - support for updating multiple entities in one batch
  - syntax in parameter values: >, <, >=, <=, <=> , (!)IS NULL, (!)IS BLANK, (!)IS EMPTY and ! at start of input)
  - multi lingual
  - theming
  - strict separation frontend / backend and services / jpa.


# building

clone repository

mvn install

# create your own application

Read the javadoc of the modules.

## services/jpa

see also https://bitbucket.org/fryske-akademy/java-architecture

### jpa model

```xml
        <dependency>
            <groupId>org.fryske-akademy</groupId>
            <artifactId>jpaservices</artifactId>
        </dependency>
```
```java
@Entity @Audited @Access(AccessType.FIELD)
public class Lemma extends AbstractEntity { .... }
```
### services module depending on jpa model

```java
@Local({CrudWriteService.class}) @Stateless
public class CrudServiceBean extends AbstractCrudServiceEnvers {

    @PersistenceContext(unitName = "my_unit")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {return em;}
}

@Local({Auditing.class}) @Stateless
public class ReadonlyServiceBean extends AbstractCrudServiceEnvers {

  @PersistenceContext(unitName = "my_ro_unit")
  private EntityManager em;

  @Override
  public EntityManager getEntityManager() {return em;}
}

```

## configuration MOVED TO https://github.com/eduarddrenth/Configuration:

include a dependency on com.vectorprint:Config  
create a class to configure settings like this:

```java
	@ApplicationScoped
	public class ConfigUrl {
	    
	    @Produces @ConfigFileUrl
	    public String getConfigUrl() { return "/etc/my.properties"; }
	
	    @Produces @FromJar
	    public Boolean getReadFromJar() { return false; }

	    @Produces @AutoReload
        public Boolean getAutoReload() { return true; }

        @Produces @POLL_INTERVAL
        public int pollInterval() {return 10;}
	}
```
create a properties file (preferably outside of deployment)  
annotate fields, methods and parameters as properties, like this:
```java
	    @Inject
	    private void setTest(@Property(keys = {"test"}, required = true) String test) {this.test=test};
```

**NOTE!!** reloading properties will work for method parameters, for statics, in @Singleton EJB and for @Properties

## gui module depending on services

```xml
        <dependency>
            <groupId>org.fryske-akademy</groupId>
            <artifactId>primefacesgui</artifactId>
        </dependency>
```

```java
public abstract class MyLazy<T extends EntityInterface> extends NewSupportingLazyModel<T> {

  @Inject
  private transient CrudReadService crudReadService;

  @Serial
  private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
    inputStream.defaultReadObject();
    auditing = CDI.current().select(CrudReadService.class).get();
  }

  @Override
  public CrudReadService getCrudReadService() {return auditing;}

}

@Named @SessionScoped
public class LazyLemma extends MyLazy<Lemma> { .... }

public abstract class MyController<T extends AbstractEntity, R extends NewSupportingLazyModel<T>> extends NewSupportingLazyController<T, R> {

  @Inject
  private transient CrudWriteService crudWriteService;
  @Inject
  private transient Auditing auditing;

  @Serial
  private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
    inputStream.defaultReadObject();
    crudWriteService = CDI.current().select(CrudWriteService.class).get();
    auditing = CDI.current().select(Auditing.class).get();
  }
}

@SessionScoped @Named
public class LemmaController extends MyController<Lemma, LazyLemma> { .... }

@Named(value = "sessionBean") @SessionScoped
public class StwdSessionBean extends SessionBean { .... }
```
```xml
<p:dataTable allowUnsorting="true" filterEvent="change" id="datalist" value="#{myController.lazyModel}" var="lemma"
            selection="#{myController.selected}" sortMode="multiple" selectionMode="single"
            paginator="true" editable="#{sessionBean.mayEdit()}" editingRow="#{sessionBean.mayEdit()}"
            rowKey="#{myController.getRowKey(lemma.id)}" filteredValue="#{lazyLemma.filtered}"
            rows="10" lazy="true" draggableColumns="true" resizableColumns="true" liveResize="true" widgetVar="datalist"
            rowsPerPageTemplate="10,20,30,40,50" multiViewState="#{myController.rememberTableState}"
            currentPageReportTemplate="{startRecord}-{endRecord} of {totalRecords}"
            paginatorTemplate="{RowsPerPageDropdown} {CurrentPageReport} {FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink}"
>
    <p:column class="row-editor" exportable="false" rendered="#{sessionBean.mayEdit()}">
      <p:rowEditor />
    </p:column>
  .
  .
   <p:column sortBy="#{lemma.form}" filterBy="#{lemma.form}" filterValue="#{lazyLemma.filters['form']}" headerText="form">
      <p:cellEditor>
        <f:facet name="output">
          <h:outputText value="#{lemma.form}"/>
        </f:facet>
        <f:facet name="input">
          <p:inputText onchange="changed($(this))" id="form" value="#{lemma.form}"  />
        </f:facet>
      </p:cellEditor>
    </p:column>
</p:dataTable>

<faces-config>
  .
  .
        <el-resolver>org.primefaces.application.exceptionhandler.PrimeExceptionHandlerELResolver</el-resolver>
        <action-listener>org.primefaces.application.DialogActionListener</action-listener>
        <navigation-handler>org.primefaces.application.DialogNavigationHandler</navigation-handler>
        <view-handler>org.primefaces.application.DialogViewHandler</view-handler>
    </application>
    <factory>
        <exception-handler-factory>org.fryske_akademy.jsf.exceptions.DeepestCauseExceptionHandlerFactory</exception-handler-factory>
    </factory>
    <lifecycle><phase-listener>org.fa.foarkarswurdlist.jsf.AjaxSessionTimeoutListener</phase-listener></lifecycle>
</faces-config>

<web-app
xmlns="https://jakarta.ee/xml/ns/jakartaee"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="https://jakarta.ee/xml/ns/jakartaee https://jakarta.ee/xml/ns/jakartaee/web-app_6_0.xsd"
version="6.0"
>
  <context-param>
    <param-name>jakarta.faces.PROJECT_STAGE</param-name>
    <param-value>Production</param-value>
  </context-param>
  <context-param>
    <param-name>primefaces.THEME</param-name>
    <param-value>#{sessionBean.currentTheme}</param-value>
  </context-param>
  <context-param>
    <param-name>primefaces.FONT_AWESOME</param-name>
    <param-value>true</param-value>
  </context-param>
  <context-param>
    <param-name>primefaces.SUBMIT</param-name>
    <param-value>partial</param-value>
  </context-param>
</web-app>
```
