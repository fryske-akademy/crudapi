/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fryske_akademy.services;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.StringConverter;
import jakarta.persistence.PersistenceException;
import org.fryske_akademy.jpa.JpqlBuilder;
import org.fryske_akademy.jpa.JpqlBuilderImpl;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author eduard
 */
public class JpqlBuilderImplTest {
    
    @Test
    public void testGrouping() {
        Param.Builder b = new Param.Builder();
        b.add("key","value").setGroupStartsLastParam(1);
        JpqlBuilder jb = new JpqlBuilderImpl();
        try {
            jb.whereClause(b.build());
            Assertions.fail("unbalanced grouping not detected");
        } catch (PersistenceException ex) {
            // expected
        }
        b.add("key2","value2").setGroupEndsLastParam(1);
        jb.whereClause(b.build());
        b.add("key3","value3");
        jb.whereClause(b.build());
    }

    @Test
    public void testnestedGrouping() {
        Param.Builder b = new Param.Builder();
        b.add("key","value").setGroupStartsLastParam(2);
        JpqlBuilder jb = new JpqlBuilderImpl();
        try {
            jb.whereClause(b.build());
            Assertions.fail("unbalanced grouping not detected");
        } catch (PersistenceException ex) {
            // expected
        }
        b.add("key2","value2").setGroupEndsLastParam(1);
        try {
            jb.whereClause(b.build());
            Assertions.fail("unbalanced grouping not detected");
        } catch (PersistenceException ex) {
            // expected
        }
        b.add("key3","value3").setGroupEndsLastParam(1);
        jb.whereClause(b.build());
    }

    @Test
    public void testBetween() {
        Param.Builder b = new Param.Builder();
        b.add("key","1 <=> 10", StringConverter.INT_PARSER);
        JpqlBuilder jb = new JpqlBuilderImpl();
        String q = jb.whereClause(b.build());
        Assertions.assertTrue(q.contains(" " + OPERATOR.BETWEEN.getToken() + " ") && q.contains(" :keyMin ") && q.contains(" :keyMax"));
        Assertions.assertNotNull(b.build().iterator().next().getMaxValue());

        b = new Param.Builder();
        b.add("key","1- 10");
        q = jb.whereClause(b.build());
        Assertions.assertTrue(q.contains(" " + OPERATOR.LIKE.getToken() + " ") && q.contains(" :key"));
        Assertions.assertNull(b.build().iterator().next().getMaxValue());

        b = new Param.Builder();
        try {
            b.add("key"," <=> 10");
            Assertions.fail("empty not allowed as values for BETWEEN");
        } catch (IllegalArgumentException ex) {
            //expected
        }

        b = new Param.Builder();
        try {
            b.add("key", "key", OPERATOR.BETWEEN, 1, false, false, null);
            Assertions.fail("null not allowed as maxValue for BETWEEN");
        } catch (IllegalArgumentException ex) {
            //expected
        }
        try {
            b.add("key", "key", OPERATOR.BETWEEN, 1, false, false, "2");
            Assertions.fail("different classes not supported for BETWEEN");
        } catch (IllegalArgumentException ex) {
            //expected
        }

    }
    
}
