/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fryske_akademy.services;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.StringConverter;
import org.fryske_akademy.jpa.JpqlBuilder;
import org.fryske_akademy.jpa.JpqlBuilderImpl;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author eduard
 */
public class ParamTest {

    
    private List<Param> build;

    @Test
    public void testSyntaxSupport() {

        init(true,null,false);
        assertTrue(build.get(0).getNot().equalsIgnoreCase(" not "));
        assertTrue("x".equals(build.get(0).getParamValue()));
        assertTrue(build.get(1).getOperator()== OPERATOR.ISNULL);
        assertTrue(build.get(2).getOperator()== OPERATOR.ISNOTNULL);
        assertTrue(build.get(2).getNot().equalsIgnoreCase(" NOT "));
        assertTrue(build.get(2).getParamValue().equals(""));
        assertTrue(build.get(3).getParamValue().equals("d"));
        assertTrue(build.get(4).getOperator()== OPERATOR.GT);
        assertTrue(build.get(4).getNot().equalsIgnoreCase(" not "));
        assertTrue(build.get(4).getParamValue().equals("is not null"));
        assertTrue(build.get(5).getOperator()== OPERATOR.LIKE);
        assertTrue(build.get(8).getOperator()== OPERATOR.GE);
    }
    
    @Test
    public void testAndOr() {
        Param.Builder b = new Param.Builder();
        build = b
                .add("a", "!x AND c <=> f")
                .add("b", "!x AND c <=> f OR >10").build();
        assertEquals(5, build.size());
        assertEquals(Param.AndOr.AND, build.get(0).getAndOr());
        assertEquals(Param.AndOr.AND, build.get(1).getAndOr());
        assertEquals(Param.AndOr.AND, build.get(2).getAndOr());
        assertEquals(Param.AndOr.AND, build.get(3).getAndOr());
        assertEquals(Param.AndOr.OR, build.get(4).getAndOr());
        JpqlBuilder jpqlBuilder = new JpqlBuilderImpl();
        jpqlBuilder.whereClause(build);
        build = b.clear()
                .add("a", "!x AND c <=> f").build();
        assertEquals(2, build.size());
        assertEquals(Param.AndOr.AND, build.get(1).getAndOr());
        jpqlBuilder.whereClause(build);
    }

    private void init(boolean syntax, Param.Builder.WildcardMapping wildcardMapping, boolean casInsensitive) {
        Param.Builder b = new Param.Builder(syntax,wildcardMapping, casInsensitive);
        build = b
                .add("a", "!x")
                .add("b", "b", "=", "iS nUll",true,null)
                .add("c", "!is null")
                .add("d", "d")
                .add("e", "!>is not null")
                .add("g", "?f*")
                .add("obj",new Object(),false)
                .add("string","*string*",false)
                .add("h", ">=6")
                .build();
    }

    @Test
    public void testNoSyntaxSupport() {
        init(false,null, false);
        assertTrue(build.get(0).getNot().equalsIgnoreCase(" "));
        assertTrue(build.get(1).getOperator()== OPERATOR.EQ);
        assertTrue(build.get(3).getParamValue().equals("d"));
        assertTrue(build.get(4).getOperator()== OPERATOR.LIKE);
        assertTrue(build.get(4).getNot().equalsIgnoreCase(" "));
        assertTrue(build.get(5).getOperator()== OPERATOR.LIKE);
        assertTrue(build.get(5).getParamValue().equals("?f*"));
    }
    
        @Test
    public void testNoNull() {
        try {
            Param.one("k", null);
            fail("null should not be allowed");
        } catch (IllegalArgumentException ex) {
            
        }
    }
    
    @Test
    public void testCasInsensitive() {
        init(false,null,true);
        assertTrue(build.get(0).isCaseInsensitive());
        init(false,null,false);
        assertFalse(build.get(0).isCaseInsensitive());
    }

    @Test
    public void testWildcardMapping() {
        init(false,Param.Builder.DEFAULT_MAPPING, false);
        assertTrue(build.get(3).getParamValue().equals("d"));
        assertTrue(build.get(4).getOperator()== OPERATOR.LIKE);
        assertTrue(build.get(5).getOperator()== OPERATOR.LIKE);
        assertTrue(build.get(5).getParamValue().equals("_f%"));
        assertTrue(build.get(6).getOperator()== OPERATOR.EQ);
        assertTrue(build.get(7).getOperator()== OPERATOR.LIKE);
        assertTrue(build.get(7).getParamValue().equals("%string%"));
    }
    
    @Test
    public void testConverters() {
        List<Param> pars = new Param.Builder()
                .add("a", "300", StringConverter.INT_PARSER)
                .add("e","2021-01-02T03:34:12", StringConverter.LOCAL_DATE_TIME_PARSER)
                .build();
        assertTrue(pars.get(0).getParamValue() instanceof Integer && ((Integer)pars.get(0).getParamValue()) == 300);
        assertTrue(pars.get(1).getParamValue() instanceof LocalDateTime);
    }

    @Test
    public void testNullValues() {
        new Param.Builder()
                .add("reacties","reacties", OPERATOR.ISEMPTY.getToken(), null,false,false,null)
                .build();
    }
}
