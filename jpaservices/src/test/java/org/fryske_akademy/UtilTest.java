/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fryske_akademy;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.fryske_akademy.jpa.OPERATOR;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 *
 * @author eduard
 */
public class UtilTest {
    
    private T t;
    private S s;
    
    @Test
    public void testEnumFunctions() {
        UtilTest o = new UtilTest();
        o.s = S.S;
        assertTrue(Util.getEnumWithValue(o) == S.S);
        List<Enum> listEnums = Util.listEnums(UtilTest.class);
        assertTrue(listEnums.contains(T.S));
        assertTrue(listEnums.contains(T.T));
        assertTrue(listEnums.contains(S.S));
        UtilTest setEnumsFromString = Util.setEnumsFromString("t.t  s.S", new UtilTest());
        assertTrue(setEnumsFromString.s==S.S);
        assertTrue(setEnumsFromString.t==T.T);
        assertTrue(Util.findInClass(UtilTest.class, "t.t")==T.T);
        Util.setEnum(T.T, o);
        assertTrue(o.t==T.T);
        
        assertFalse(Util.hasSetterForEnumString("a.a", o));
        assertTrue(Util.hasSetterForEnumString("t.t", o));
        
    }
    
    @Test
    public void testSplit() {
        assertTrue(Util.split("a:b", 0).equals("a"));
        assertTrue(Util.split("a: b", 0).equals("a"));
        assertTrue(Util.split("a: b", 3)==null);
        assertTrue(Util.split("a: b:c", 1,true).equals("b:c"));
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public S S() {
        return s;
    }

    public void setS(S s) {
        this.s = s;
    }

    @Test
    void wrapStringInWildCards() {
        assertEquals("*aap*",Util.wrapStringInWildCards("aap",true));
        assertEquals("aa*p",Util.wrapStringInWildCards("aa*p",true));
        assertEquals("\"aap\"",Util.wrapStringInWildCards("\"aap\"",true));
        assertEquals("aap OR beer",Util.wrapStringInWildCards("aap OR beer",true));
        assertEquals(">aap",Util.wrapStringInWildCards(">aap",true));
        assertEquals("!>aap",Util.wrapStringInWildCards("!>aap",true));
        assertEquals("<=aap",Util.wrapStringInWildCards("<=aap",true));
        assertEquals("!<=aap",Util.wrapStringInWildCards("!<=aap",true));
        assertEquals("aap <=> beer",Util.wrapStringInWildCards("aap <=> beer",true));
        assertFalse(OPERATOR.operatorInUserInput("*aap <> beer*"));
        assertEquals("*aap <> beer*",Util.wrapStringInWildCards("aap <> beer",true));
        assertEquals("*aap OR beer*",Util.wrapStringInWildCards("aap OR beer",false));
    }

    public enum T { T, S }
    public enum S { S }
    
    
    
}
