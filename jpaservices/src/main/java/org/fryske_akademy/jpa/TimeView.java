package org.fryske_akademy.jpa;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;

import java.time.LocalDateTime;

/**
 * class to enable LocalDateTime based querying of auditing information.
 * Accompanying postgres view, of course you may as well add a timestamp column that will be filled from
 * your RevisionListener.
 * <pre>
 create view timeview as
    select id, username,
    (to_timestamp('01-01-1970 00:00:00', 'DD-MM-YYYY HH24:MI:SS') + timestamp * interval '1 millisecond') at time zone 'cest' at time zone 'utc'
 as timestamp from revisioninfo;

 create index revuser on revisioninfo(username);
 create index revtime on revisioninfo(timestamp);
</pre>
 */
@NamedQuery(name = TimeView.USERS,
        query = "select distinct(t.username) from TimeView t"
)
@Entity
@Cacheable
public class TimeView implements EntityInterface {
    public static final String USERS = "users";
    @Id
    @Column
    private Integer id;
    @Column
    private String username;

    @Column
    private LocalDateTime timestamp;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean isTransient() {
        return false;
    }

    @Override
    public String toString() {
        return "TimeWindow{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
