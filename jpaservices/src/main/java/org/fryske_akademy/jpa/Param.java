/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.jpa;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.StringConverter;
import org.fryske_akademy.Util;
import org.fryske_akademy.services.CrudReadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Holder for parameter info used when {@link JpqlBuilder building} a jpql
 * query. A {@link Builder#Builder() } (with syntax support by default) is
 * responsible for yielding parameters. Intelligence is in the Builder, not in
 * the Param. NOTE you can set logging level to DEBUG for trouble shooting.
 *
 * @see OPERATOR
 *
 * @author eduard
 */
public class Param {

    private static final Logger LOGGER = LoggerFactory.getLogger(Param.class.getName());

    /**
     * Call
     * {@link #one(java.lang.String, java.lang.String, java.lang.Object, boolean, org.fryske_akademy.jpa.Param.Builder.WildcardMapping, boolean) }
     * with true, {@link Builder#DEFAULT_MAPPING} and false
     *
     * @param key
     * @param value
     * @return
     */
    public static List<Param> one(String key, Object value) {
        return one(key, key, value, true, Builder.DEFAULT_MAPPING, false);
    }

    /**
     * Calls
     * {@link Builder#add(String, String, OPERATOR, Object, boolean, boolean, Object)}
     * with key,key,operator,value,false,false,null
     *
     * @param key
     * @param operator
     * @param value
     * @return
     */
    public static List<Param> one(String key, OPERATOR operator, Object value) {
        return new Builder().add(key, key, operator, value, false, false, null).build();
    }

    /**
     * Call
     * {@link #one(java.lang.String, java.lang.String, java.lang.Object, boolean, org.fryske_akademy.jpa.Param.Builder.WildcardMapping, boolean) }
     * with true, {@link Builder#DEFAULT_MAPPING} and false
     *
     * @param propertyPath The propertyPath may differ from key allowing you to
     * apply multiple comparisons for the same propertyPath (i.e.
     * e.column1=:column1 or e.column1=:column2).
     * @param key
     * @param value
     * @return
     */
    public static List<Param> one(String propertyPath, String key, Object value) {
        return one(propertyPath, key, value, true, Builder.DEFAULT_MAPPING, false);
    }

    /**
     * Calls {@link Builder#add(java.lang.String, java.lang.String, com.vectorprint.StringConverter) }
     * }
     *
     * @param key
     * @param value
     * @param converter
     * @return
     */
    public static List<Param> one(String key, String value, StringConverter converter) {
        return new Builder().add(key, value, converter).build();
    }

    /**
     * Calls {@link Builder#add(java.lang.String, java.lang.String, java.lang.Object, boolean)
     * } with false
     *
     * @param propertyPath The propertyPath may differ from key allowing you to
     * apply multiple comparisons for the same propertyPath (i.e.
     * e.column1=:column1 or e.column1=:column2).
     * @param key
     * @param value
     * @param syntaxSupport
     * @param wildcardMapping
     * @param caseInsensitive
     * @return
     */
    public static List<Param> one(String propertyPath, String key, Object value, boolean syntaxSupport, Builder.WildcardMapping wildcardMapping, boolean caseInsensitive) {
        List<Param> params = new Builder(syntaxSupport, wildcardMapping, caseInsensitive).add(propertyPath, key, value, false).build();
        if (params.size()>1) {
            LOGGER.warn(String.format("Syntax support yielded %d parameters! This is only supported in dynamic queries (%s).",
                    params.size(), CrudReadService.class.getSimpleName()+"#*Dynamic"
            ));
        }
        return params;
    }

    private final String propertyPath;
    private final String paramKey;
    private final OPERATOR operator;
    private final Object paramValue;
    private final String not;
    private final AndOr andOr;
    private final boolean caseInsensitive;
    private final Object maxValue;
    private int groupStarts;
    private int groupEnds;

    /**
     * Bottleneck constructor
     *
     * @param propertyPath The propertyPath may differ from key allowing you to
     * apply multiple comparisons for the same propertyPath (i.e.
     * e.column1=:column1 or e.column1=:column2).
     * @param paramKey the key of the parameter i.e. :id
     * @param operator the operator i.e. like or =
     * @param paramValue may not be null
     * @param not when true use negation
     * @param or when true use or otherwise and
     * @param caseInsensitive when true query case insensitive
     */
    private Param(String propertyPath, String paramKey, OPERATOR operator, Object paramValue, boolean not, boolean or, boolean caseInsensitive, Object maxValue) {
        switch (operator) {
            case ISNOTEMPTY:
            case ISEMPTY:
            case ISNOTNULL:
            case ISNULL:
                break;
            case BETWEEN:
                if (maxValue == null) {
                    throw new IllegalArgumentException(paramKey + ": maxValue may not be null for BETWEEN");
                } else if (!maxValue.getClass().isInstance(paramValue)) {
                    throw new IllegalArgumentException(paramKey + ": maxValue and paramValue must be the same class for BETWEEN");
                } else if (paramValue instanceof String s && (Util.nullOrEmpty(s.trim()) || Util.nullOrEmpty(String.valueOf(maxValue).trim()))) {
                    throw new IllegalArgumentException(paramKey + ": trimmed maxValue and paramValue may not be empty for BETWEEN");
                }
            default:
                if (paramValue == null) {
                    throw new IllegalArgumentException(paramKey + ": paramValue may not be null");
                }
        }
        this.propertyPath = propertyPath + " ";
        this.paramKey = paramKey;
        this.operator = operator;
        this.paramValue = paramValue;
        this.not = not ? " NOT " : " ";
        this.andOr = AndOr.fromBool(or);
        this.caseInsensitive = caseInsensitive;
        this.maxValue = maxValue;
    }

    public String getPropertyPath() {
        return propertyPath;
    }

    /**
     * for native queries this key should be a numeric positional parameter
     *
     * @return
     */
    public String getParamKey() {
        return paramKey;
    }

    public OPERATOR getOperator() {
        return operator;
    }

    public Object getParamValue() {
        return paramValue;
    }

    public String getNot() {
        return not;
    }

    /**
     * Will be prepended in the query for parameters except the first
     *
     * @return
     */
    public AndOr getAndOr() {
        return andOr;
    }

    public Class getParamType() {
        return paramValue.getClass();
    }

    public Object getMaxValue() {
        return maxValue;
    }

    public boolean isCaseInsensitive() {
        return caseInsensitive;
    }

    /**
     * how many groups does this parameter start, "(";
     *
     * @return
     */
    public int getGroupStarts() {
        return groupStarts;
    }

    /**
     * how many groups does this parameter end, ")";
     *
     * @return
     */
    public int getGroupEnds() {
        return groupEnds;
    }

    public void setGroupStarts(int groupStarts) {
        this.groupStarts = groupStarts;
    }

    public void setGroupEnds(int groupEnds) {
        this.groupEnds = groupEnds;
    }

    @Override
    public String toString() {
        return "Param{"
                + "propertyPath='" + propertyPath + '\''
                + ", paramKey='" + paramKey + '\''
                + ", operator='" + operator + '\''
                + ", paramValue=" + paramValue
                + ", not='" + not + '\''
                + ", andOr=" + andOr
                + ", caseInsensitive=" + caseInsensitive
                + ", startGroup=" + groupStarts
                + ", endGroup=" + groupEnds
                + ", maxValue=" + maxValue
                + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.paramKey);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Param other = (Param) obj;
        return Objects.equals(this.paramKey, other.paramKey);
    }

    /**
     * A factory for Param objects, parameter values can be added in two ways:
     * <ul>
     * <li>as (the correct) Object => only syntax and wildcard support for
     * String values</li>
     * <li>as a String optionally using a converter => syntax support also for non String
     * values, no wildcard support when converter is used</li>
     * </ul>
     * <p>
     * Users may use syntax in values they submit, see {@link OPERATOR}
     * <ul>
     * <li>wildcards: * and ?</li>
     * <li>!</li>
     * <li>(!)is null</li>
     * <li>(!)is empty</li>
     * <li>(!)is blank</li>
     * <li>(!)>(=)</li>
     * <li>(!)&lt;(=)</li>
     * <li>1 &lt;=> 10</li>
     * <li>1 &lt;=> 10 AND >12</li>
     * <li>&lt; 3 OR >6</li>
     * </ul>
     * </p>
     */
    public static class Builder {

        public static final WildcardMapping DEFAULT_MAPPING = new DefaultWildcardMapping();
        private final List<Param> params = new ArrayList<>(3);
        private final boolean syntaxInValue;
        private final WildcardMapping wildcardMapping;
        private final boolean caseInsensitive;

        public Builder(boolean syntaxInValue, WildcardMapping wildcardMapping, boolean caseInsensitive) {
            this.syntaxInValue = syntaxInValue;
            this.wildcardMapping = wildcardMapping;
            this.caseInsensitive = caseInsensitive;
        }

        /**
         * Builder with syntax support in value (!, >[=], &lt;[=], [!]is null,
         * [!]is empty, [!]is blank). Calls {@link #Builder(boolean, WildcardMapping, boolean)
         * } with true, null and false.
         */
        public Builder() {
            this(true, null, false);
        }

        /**
         * Calls {@link #Builder(boolean, WildcardMapping, boolean)}
         * } with true, null and the caseInsensitive argument.
         *
         * @param caseInsensitive
         */
        public Builder(boolean caseInsensitive) {
            this(true, null, caseInsensitive);
        }

        /**
         * Calls {@link #add(java.lang.String, java.lang.Object, boolean) }
         *
         * @param paramKey
         * @param paramValue
         * @return
         */
        public Builder add(String paramKey, Object paramValue) {
            return add(paramKey, paramValue, false);
        }

        /**
         * Calls {@link #add(java.lang.String, java.lang.String, java.lang.Object, boolean) }
         *
         * @param paramKey
         * @param paramValue
         * @param or
         * @return
         */
        public Builder add(String paramKey, Object paramValue, boolean or) {
            return add(paramKey, paramKey, paramValue, or);
        }

        /**
         * Calls {@link #add(java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, com.vectorprint.StringConverter) } or
         * {@link #add(java.lang.String, java.lang.String, org.fryske_akademy.jpa.OPERATOR, java.lang.Object, boolean, boolean, java.lang.Object) }
         *
         * @param propertyPath
         * @param paramKey
         * @param paramValue
         * @param or
         * first
         * @return
         */
        public Builder add(String propertyPath, String paramKey, Object paramValue, boolean or) {
            if (paramValue instanceof String) {
                return add(propertyPath, paramKey, "like", (String) paramValue, or, null);
            } else {
                return add(propertyPath, paramKey, OPERATOR.EQ, paramValue, false, or, null);
            }
        }

        /**
         * Calls {@link #add(java.lang.String, java.lang.String, java.lang.String, com.vectorprint.StringConverter) }
         *
         * @param paramKey
         * @param paramValue
         * @param converter see {@link StringConverter}
         * @return
         */
        public Builder add(String paramKey, String paramValue, StringConverter converter) {
            return add(paramKey, paramKey, paramValue, converter);
        }

        /**
         * Calls {@link #add(java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, com.vectorprint.StringConverter) }
         * 
         * @param propertyPath
         * @param paramKey
         * @param paramValue
         * @param converter see {@link StringConverter}
         * @return
         */
        public Builder add(String propertyPath, String paramKey, String paramValue, StringConverter converter) {
            return add(propertyPath, paramKey, converter == null ? "like" : "=", paramValue, false, converter);
        }

        /**
         * Calls {@link #add(java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, com.vectorprint.StringConverter) }
         *
         * @param paramKey
         * @param paramValue
         * @param or
         * @param converter see {@link StringConverter}
         * @return
         */
        public Builder add(String paramKey, String paramValue, boolean or, StringConverter converter) {
            return add(paramKey, paramKey, converter == null ? "like" : "=", paramValue, or, converter);
        }

        /**
         * Calls {@link #add(java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, com.vectorprint.StringConverter) }
         *
         * @param paramKey
         * @param paramValue
         * @param operator
         * @param or
         * @param converter
         * @return
         */
        public Builder add(String paramKey, String paramValue, String operator, boolean or, StringConverter converter) {
            return add(paramKey, paramKey, operator, paramValue, or, converter);
        }

        private record AndOrTerms(String term, boolean or, int n) {

        }

        private List<AndOrTerms> splitTerms(String value, List<AndOrTerms> andOrTerms, boolean or) {
            // NOTE this is for the next term!
            OPERATOR andOr = OPERATOR.isAnd(value) ? OPERATOR.AND : OPERATOR.OR;
            String[] terms = value.split(andOr.getUserInput(), 2);
            andOrTerms.add(new AndOrTerms(terms[0], or, andOrTerms.size()));
            if (OPERATOR.isAnd(terms[1]) || OPERATOR.isOr(terms[1])) {
                return splitTerms(terms[1], andOrTerms, andOr == OPERATOR.OR);
            }
            andOrTerms.add(new AndOrTerms(terms[1], andOr==OPERATOR.OR, andOrTerms.size()));
            return andOrTerms;
        }

        /**
         * Bottleneck method for String values, applies syntax support and wildcard mapping when configured in the Builder.
         * Applies conversion when a converter is supplied. NOTE that
         * when using AND or OR in paramValue and syntax support is enabled, multiple parameters will be added, where
         * the name of the key is the concatenation of the original key with the index of
         * the parameter added, starting at 0.
         *
         * Calls {@link #add(String, String, OPERATOR, Object, boolean, boolean, Object)}
         * }
         * with {@link OPERATOR#operator(String, String, boolean) }
         * for the operator. The value will be the empty string when {@link OPERATOR#valueIsOperator(String, boolean)
         * } is true, will be {@link StringConverter#convert(java.lang.String) }
         * when a converter is provided (negation is stripped), otherwise the
         * value is returned with wildcards replaced and negation stripped
         * (provided syntax support in values is active).
         *
         * @param propertyPath The propertyPath may differ from key allowing you
         * to apply multiple comparisons for the same propertyPath (i.e.
         * e.column1=:column1 or e.column1=:column2).
         * @param paramKey
         * @param operator
         * @param paramValue
         * @param or
         * @param converter see {@link StringConverter}
         * @return
         */
        public Builder add(String propertyPath, String paramKey, String operator, String paramValue, boolean or, StringConverter converter) {
            if (OPERATOR.isAnd(paramValue) || OPERATOR.isOr(paramValue)) {
                splitTerms(paramValue, new ArrayList<>(2), or).forEach(term
                        -> {
                    add(propertyPath, paramKey + term.n, operator, term.term, term.or, converter);
                    if (term.n == 0) {
                        setGroupStartsLastParam(1);
                    }
                }
                );
                setGroupEndsLastParam(1);
            } else if (OPERATOR.isBetween(paramValue)) {
                /*
            paramValue may be: x <=> y
                 */
                String[] minMax = paramValue.split(OPERATOR.BETWEEN.getUserInput());
                Object min = getValue(minMax[0], converter);
                Object max = getValue(minMax[1], converter);
                if (min != null && max != null) {
                    add(propertyPath, paramKey, OPERATOR.BETWEEN, min, isNegation(paramValue), or, max);
                } else {
                    LOGGER.warn(String.format("skip adding param, min or max is null, probably %s cannot be converted by %s", min + " or " + max, converter));
                }

            } else {
                Object value = getValue(paramValue, converter);
                if (value != null) {
                    add(propertyPath, paramKey, OPERATOR.operator(operator, paramValue, syntaxInValue), value,
                            isNegation(paramValue), or, null);
                } else {
                    LOGGER.warn(String.format("skip adding param, value is null, perhaps %s cannot be converted by %s", paramValue, converter));
                }

            }
            return this;
        }

        /**
         * Add a {@link Param} that checks if propertyPath is not null
         * @param propertyPath
         * @return
         */
        public Builder checkNotNull(String propertyPath) {
            return add(propertyPath, propertyPath, OPERATOR.ISNOTNULL, "", false, false, null);
        }

        /**
         * how many groups does the last parameter start, "(";
         *
         * @return
         */
        public Builder setGroupStartsLastParam(int group) {
            return setGroupStartsParam(group,params.size()-1);
        }

        public Builder setGroupStartsParam(int group, String key) {
            params.stream().filter(p->p.getParamKey().equals(key)).findFirst().ifPresent(p->p.setGroupStarts(group));
            return this;
        }

        public Builder setGroupStartsParam(int group, int paramIndex) {
            if (params.size() > paramIndex) {
                params.get(paramIndex).setGroupStarts(group);
            }
            return this;
        }

        /**
         * how many groups does the last parameter end, ")";
         *
         * @return
         */
        public Builder setGroupEndsLastParam(int group) {
            return setGroupEndsParam(group,params.size()-1);
        }

        public Builder setGroupEndsParam(int group, int paramIndex) {
            if (params.size() > paramIndex) {
                params.get(paramIndex).setGroupEnds(group);
            }
            return this;
        }

        public Builder setGroupEndsParam(int group, String key) {
            params.stream().filter(p->p.getParamKey().equals(key)).findFirst().ifPresent(p->p.setGroupEnds(group));
            return this;
        }

        private Object getValue(String value, StringConverter converter) {
            if (OPERATOR.valueIsOperator(value, syntaxInValue)) {
                return ""; // value doesn't matter here, not used
            } else if (converter == null) {
                return replaceWildcards(OPERATOR.stripSyntax(value));
            } else {
                return converter.convert(OPERATOR.stripSyntax(value));
            }
        }

        /**
         * Bottleneck method, adds a new Param, does not apply any intelligence.
         *
         * @param propertyPath The propertyPath may differ from key allowing you
         * to apply multiple comparisons for the same propertyPath (i.e.
         * e.column1=:column1 or e.column1=:column2).
         * @param paramKey
         * @param operator
         * @param paramValue
         * @param not
         * @param or
         * @param maxValue for {@link OPERATOR#BETWEEN}
         * @throws IllegalArgumentException when paramKey is already present
         * @return
         */
        public Builder add(String propertyPath, String paramKey, OPERATOR operator, Object paramValue, boolean not, boolean or, Object maxValue) {
            add(new Param(propertyPath, paramKey, operator, paramValue, not, or, caseInsensitive, maxValue));
            return this;
        }

        /**
         * Calls
         * {@link #add(String, String, OPERATOR, Object, boolean, boolean, Object)}
         * with null for maxValue
         *
         * @param propertyPath
         * @param paramKey
         * @param operator
         * @param paramValue
         * @param not
         * @param or
         * @return
         */
        public Builder add(String propertyPath, String paramKey, OPERATOR operator, Object paramValue, boolean not, boolean or) {
            add(propertyPath, paramKey, operator, paramValue, not, or, null);
            return this;
        }

        /**
         * Calls
         * {@link #add(String, String, OPERATOR, Object, boolean, boolean, Object)}
         * with {@link OPERATOR#fromToken(String)}
         *
         * @param propertyPath
         * @param paramKey
         * @param operator
         * @param paramValue
         * @param not
         * @param or
         * @param maxValue for {@link OPERATOR#BETWEEN}
         * @return
         */
        public Builder add(String propertyPath, String paramKey, String operator, Object paramValue, boolean not, boolean or, Object maxValue) {
            add(new Param(propertyPath, paramKey, OPERATOR.fromToken(operator), paramValue, not, or, caseInsensitive, maxValue));
            return this;
        }

        /**
         * Calls
         * {@link #add(String, String, String, Object, boolean, boolean, Object)}
         * with null for maxValue
         *
         * @param propertyPath
         * @param paramKey
         * @param operator
         * @param paramValue
         * @param not
         * @param or
         * @return
         */
        public Builder add(String propertyPath, String paramKey, String operator, Object paramValue, boolean not, boolean or) {
            add(propertyPath, paramKey, operator, paramValue, not, or, null);
            return this;
        }

        private void add(Param param) {
            if (params.stream().anyMatch(p -> p.paramKey.equals(param.paramKey))) {
                throw new IllegalArgumentException(String.format("builder already contains %s", param.paramKey));
            }
            params.add(param);
        }

        public Builder remove(String paramKey) {
            if (params.stream().anyMatch((p) -> p.paramKey.equals(paramKey))) {
                for (Iterator<Param> iterator = params.iterator(); iterator.hasNext();) {
                    Param next = iterator.next();
                    if (next.paramKey.equals(paramKey)) {
                        iterator.remove();
                        break;
                    }
                }
            }
            return this;
        }

        public Builder clear() {
            params.clear();
            return this;
        }

        /**
         * check if a string (user value) indicates a negation when
         * syntaxInValue is used
         *
         * @see OPERATOR#negation(String)
         * @param value
         * @return
         */
        public boolean isNegation(String value) {
            return syntaxInValue && OPERATOR.negation(value);
        }

        private String replaceWildcards(String value) {
            return wildcardMapping == null ? value : value
                    .replace(wildcardMapping.getZeroOrMoreInput(), wildcardMapping.getZeroOrMoreOutput())
                    .replace(wildcardMapping.getZeroOrOneInput(), wildcardMapping.getZeroOrOneOutput());
        }

        public Builder addParam(List<Param> p) {
            p.forEach(this::add);
            return this;
        }

        public Builder addParam(Param... p) {
            Stream.of(p).forEach(this::add);
            return this;
        }

        /**
         * Also useful if for example you need to add parameters yielded by another builder
         *
         * @return
         */
        public List<Param> build() {
            return params;
        }

        /**
         * when true user input is parsed for syntax, see {@link OPERATOR}
         *
         * @return
         */
        public boolean isSyntaxInValue() {
            return syntaxInValue;
        }

        public boolean containsKey(String key) {
            return params.stream().anyMatch((t) -> t.getParamKey().equals(key));
        }

        /**
         * * =&gt; %, ? =&gt; _, NOTE that this mapping causes all four
         * characters to be interpreted as wildcards in jpql/sql    .
         */
        public static class DefaultWildcardMapping implements WildcardMapping {

            @Override
            public char getZeroOrMoreInput() {
                return '*';
            }

            @Override
            public char getZeroOrMoreOutput() {
                return '%';
            }

            @Override
            public char getZeroOrOneInput() {
                return '?';
            }

            @Override
            public char getZeroOrOneOutput() {
                return '_';
            }

        }

        /**
         * translation table for wildcards
         */
        public interface WildcardMapping {

            char getZeroOrMoreInput();

            char getZeroOrMoreOutput();

            char getZeroOrOneInput();

            char getZeroOrOneOutput();
        }

    }

    public enum AndOr {
        AND, OR;

        private static AndOr fromBool(boolean or) {
            return or ? OR : AND;
        }

        @Override
        public String toString() {
            return " " + name() + " ";
        }

    }

}
