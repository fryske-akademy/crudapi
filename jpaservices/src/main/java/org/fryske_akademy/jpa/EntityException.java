/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fryske_akademy.jpa;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.persistence.PersistenceException;

import java.util.Optional;

/**
 * An Exception that tracks the entity that caused the failure, useful for
 * example the combination of EntityListeners and batch operations.
 *
 * @see org.fryske_akademy.services.CrudWriteService#batchSave(java.util.Collection, java.lang.Integer)
 * @see org.fryske_akademy.services.CrudWriteService#batchDelete(java.util.Collection, java.lang.Integer)
 * @author eduard
 */
public class EntityException extends PersistenceException {
    
    /**
     * get hold of an entity in the exception chain.
     * @param ex
     * @return 
     */
    public static Optional<EntityInterface> fromException(Throwable ex) {
        if (ex instanceof EntityException) {
            return Optional.of(((EntityException)ex).failed);
        } else if (ex == null) {
            return Optional.empty();
        }
        return fromException(ex.getCause());
        
    }

    private final EntityInterface failed;

    public EntityException(EntityInterface failed) {
        this.failed = failed;
    }

    public EntityException(EntityInterface failed, String message) {
        super(message);
        this.failed = failed;
    }

    public EntityException(EntityInterface failed, Exception ex) {
        super(ex);
        this.failed = failed;
    }

    public EntityException(EntityInterface failed, String message, Exception ex) {
        super(message, ex);
        this.failed = failed;
    }

    public EntityInterface getFailed() {
        return failed;
    }

}
