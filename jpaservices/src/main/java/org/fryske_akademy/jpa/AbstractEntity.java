/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.jpa;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;
import jakarta.persistence.Version;

import java.util.Objects;

/**
 * superclass for entities, defines auto generated id ({@link GenerationType#IDENTITY}), a version column and a function to check if the entity is transient (not in db yet).
 * Jpa annotations are on getters which allows you to override. If you want to annotate fields in your subclasses be sure to annotate with
 * {@link AccessType#FIELD}.
 * @author eduard
 */
@MappedSuperclass
@Access(AccessType.PROPERTY)
public class AbstractEntity implements EntityInterface {
    
    private Integer id;
    private int version = -1;

    public AbstractEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Version
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        return 13 * 3 + (this.id != null ? this.id.hashCode() : 0);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractEntity other = (AbstractEntity) obj;
        return Objects.equals(this.id, other.id );
    }
    
    /**
     * is the entity in the db?
     * @return true when version &lt; 0 (version default value is -1) or id is null
     */
    @Transient
    @Override
    public boolean isTransient() {
        return version < 0 || id == null;
    }
    

}
