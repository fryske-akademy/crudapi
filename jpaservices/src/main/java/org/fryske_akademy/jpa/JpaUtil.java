package org.fryske_akademy.jpa;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.security.enterprise.SecurityContext;
import jakarta.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class JpaUtil {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(JpaUtil.class.getName());
    
    private static final Map<String, EntityManagerFactory> emFacts
            = new ConcurrentHashMap<>(5) {
        @Override
        public EntityManagerFactory get(Object key) {
            if (!containsKey(key) || !super.get(key).isOpen()) {
                put(String.valueOf(key), Persistence.createEntityManagerFactory(String.valueOf(key)));
            }
            return super.get(key);
        }
    };
    
    /**
     * return a cached EntityManagerFactory, make sure you close it when it is not needed anymore.
     * @param name
     * @return 
     */
    public static EntityManagerFactory getEntityManagerFactory(String name) {
        return emFacts.get(name);
    }

    /**
     * Looks for principal in CDI
     *
     * @return
     * @throws IllegalStateException
     */
    public static Principal findPrincipal() throws IllegalStateException {
        return CDI.current().select(SecurityContext.class).get().getCallerPrincipal();
    }
    
    public static <T> List<T> findAll(Class<T> type, EntityManager entityManager) {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(type));
        return entityManager.createQuery(cq).getResultList();
    }
    
    /**
     * looks for an EntityInterface in a EntityException or ConstraintViolationException
     * (in the first ConstraintViolation).
     * @param ex
     * @return an EntityInterface or empty optional
     */
    public static Optional<EntityInterface> fromException(Throwable ex) {
        if (ex instanceof EntityException) {
            return EntityException.fromException(ex);
        } else if (ex instanceof ConstraintViolationException cve) {
            if (!cve.getConstraintViolations().isEmpty()) {
                Object rootBean = cve.getConstraintViolations()
                        .iterator().next().getRootBean();
                return rootBean instanceof EntityInterface ei ? Optional.of(ei) : Optional.empty();
            }
        }
        return Optional.empty();
    }
    
}
