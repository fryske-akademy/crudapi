/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fryske_akademy.jpa;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.persistence.Query;
import org.fryske_akademy.services.CrudReadService;

import java.util.List;
import java.util.Map;

/**
 * Responsible for building where and order by clauses in jpql
 *
 * @author eduard
 */
public interface JpqlBuilder {


    /**
     *
     * builds an order by clause
     *
     * @param sort
     * @return
     */
    String orderClause(Map<String, CrudReadService.SORTORDER> sort);

    /**
     * builds a where clause
     *
     * @see #whereCondition(Param)
     * @see #setWhereParams(jakarta.persistence.Query, java.util.List) 
     *
     * @param params
     * @return
     */
    String whereClause(List<Param> params);

    /**
     * builds a where condition
     *
     * @see #setWhereParams(jakarta.persistence.Query, java.util.List)
     *
     * @param param
     * @return
     */
    String whereCondition(Param param);

    /**
     * Fills parameters created in {@link #whereCondition(Param)}
     *
     *
     * @param q
     * @param params
     */
    void setWhereParams(Query q, List<Param> params);

    /**
     * Set the value of a jpql parameter prepared in {@link #whereCondition(Param)}
     *
     * @param q
     * @param param
     */
    void setParam(Query q, Param param);

}
