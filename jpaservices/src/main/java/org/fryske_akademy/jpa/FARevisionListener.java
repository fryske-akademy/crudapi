/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.jpa;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.enterprise.inject.spi.CDI;
import jakarta.security.enterprise.SecurityContext;
import org.hibernate.envers.RevisionListener;

/**
 *
 * @author eduard
 */
public class FARevisionListener implements RevisionListener {
    
    /**
     * when principal name not found
     */
    public static final String UNKNOWN = "UNKNOWN";
    /**
     * adds principal name or {@link #UNKNOWN} to {@link RevisionInfo}.
     * @param revisionEntity 
     */
    @Override
    public void newRevision(Object revisionEntity) {
        SecurityContext securityContext = CDI.current().select(SecurityContext.class).get();
        RevisionInfo revisioninfo = (RevisionInfo) revisionEntity;
        revisioninfo.setUsername(securityContext.getCallerPrincipal()!=null?securityContext.getCallerPrincipal().getName():UNKNOWN);
    }


}
