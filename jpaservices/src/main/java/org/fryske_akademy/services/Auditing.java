/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.services;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.validation.constraints.Positive;
import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jpa.RevInfo;

import java.io.Serializable;
import java.util.List;

/**
 * Generic interface for entity history.
 *
 * @author eduard
 */
public interface Auditing extends CrudReadService {

    /**
     * return list of revision numbers, first is the oldest
     *
     * @param id
     * @return
     */
    <T> List<Number> getRevisionNumbers(Serializable id, Class<T> type);

    /**
     * get historical data for a certain revision
     *
     * @param <T>
     * @param n
     * @param type
     * @return
     */
    <T> T getRevision(Number n, Class<T> type);

    /**
     * return a list holding revision information, newest come first
     *
     * @param <T>
     * @param id
     * @param max the maximum to number of results to return, defaults to 5
     * @param type
     * @return
     */
    <T> List<RevInfo<T>> getRevisionInfo(Serializable id, Integer max, Class<T> type);

    /**
     * Return the state before the last crud operation on an entity, or null when not found (or for insert). This can be used to build
     * undo functionality
     * @param id
     * @param type
     * @param stepsBack how may steps back in history, 1 is the first step back
     */
    <T extends EntityInterface> T previousState(Serializable id, Class<T> type, @Positive int stepsBack);

    /**
     *
     * @param e
     * @param max
     * @param <E>
     * @return
     */
    <E> List<RevInfo<E>> changes(E e, Integer max);

}
