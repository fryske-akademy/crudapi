/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.services;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.fryske_akademy.jpa.Param;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Generic interface for read operations on entities, the dynamic methods offer powerfull support for building queries
 * including a bit of syntax in user input.
 *
 * @see org.fryske_akademy.jpa.OPERATOR
 * @see org.fryske_akademy.jpa.JpqlBuilder
 * @see Param
 * @author eduard
 */
public interface CrudReadService {

    enum SORTORDER {
        DESC, ASC, NONE;

        public static Map<String, SORTORDER> order(String key, SORTORDER order) {
            return new Builder().add(key, order).build();
        }

        public static class Builder {

            private final Map<String, SORTORDER> sort = new LinkedHashMap<>(2);

            public Builder add(String key, SORTORDER o) {
                sort.put(key, o);
                return this;
            }

            public Map<String, SORTORDER> build() {
                return sort;
            }
        }
    }

    /**
     * Return a default page size when no max is given, negative value means no limit
     *
     * @return
     */
    int getDefaultPageSize();

    <T> T find(Serializable id, Class<T> type);

    <T> List<T> findAll(Class<T> type);

    <T> Stream<T> streamAll(Class<T> type);

    /**
     * 
     * @param <T>
     * @param first defaults to 0
     * @param max defaults to {@link #getDefaultPageSize() }
     * @param sort
     * @param params
     * @param type
     * @return 
     */
    <T> List<T> findDynamic(Integer first, Integer max, Map<String, SORTORDER> sort, List<Param> params, Class<T> type);

    /**
     * 
     * @param <T>
     * @param first defaults to 0
     * @param max defaults to {@link #getDefaultPageSize() }
     * @param sort
     * @param params
     * @param type
     * @return 
     */
    <T> Stream<T> streamDynamic(Integer first, Integer max, Map<String, SORTORDER> sort, List<Param> params, Class<T> type);

    /**
     * Call this using the same parameters as in findDynamic to get to know the total number of results
     * @param params
     * @param type
     * @return 
     */
    int countDynamic(List<Param> params, Class type);

    /**
     * 
     * @param <T>
     * @param namedQuery
     * @param params
     * @param first defaults to 0
     * @param max defaults to {@link #getDefaultPageSize() }
     * @param type
     * @return 
     */
    <T> List<T> find(String namedQuery, List<Param> params, Integer first, Integer max, Class<T> type);

    /**
     * query using your own jpql
     * @param jpql
     * @return
     */
    <T> List<T> findJpql(String jpql, Class<T> clazz);

    /**
     * query using your own sql and a wrapper entity class
     * @param sql
     * @return
     */
    <T> List<T> findNative(String sql, Class<T> clazz);

    /**
     * query using your own sql
     * @param sql
     * @return for select queries a list of object or object[]
     */
    List findNative(String sql);
    /**
     * 
     * @param <T>
     * @param namedQuery
     * @param params
     * @param first defaults to 0
     * @param max defaults to {@link #getDefaultPageSize() }
     * @param type
     * @return 
     */
    <T> Stream<T> stream(String namedQuery, List<Param> params, Integer first, Integer max, Class<T> type);

    /**
     * 
     * @param <T>
     * @param namedNativeQuery
     * @param params
     * @param first defaults to 0
     * @param max defaults to {@link #getDefaultPageSize() }
     * @param type
     * @return 
     */
    <T> List<T> findNative(String namedNativeQuery, List<Param> params, Integer first, Integer max, Class<T> type);

    /**
     * 
     * @param <T>
     * @param namedNativeQuery
     * @param params
     * @param first defaults to 0
     * @param max defaults to {@link #getDefaultPageSize() }
     * @param type
     * @return 
     */
    <T> Stream<T> streamNative(String namedNativeQuery, List<Param> params, Integer first, Integer max, Class<T> type);

    /**
     * Call this using the same parameters as in {@link #find(java.lang.String, java.util.List, java.lang.Integer, java.lang.Integer, java.lang.Class) }
     * to get to know the total number of results
     *
     * @param namedQuery
     * @param params
     * @return
     */
    int count(String namedQuery, List<Param> params);

    /**
     * return one result or null, multiple results throws an exception
     *
     * @param <T>
     * @param namedQuery
     * @param params
     * @param type
     * @return
     */
    <T> T findOne(String namedQuery, List<Param> params, Class<T> type);

    /**
     * return one result, no or multiple results throws an exception
     *
     * @param <T>
     * @param namedQuery
     * @param params
     * @param type
     * @return
     */
    <T> T findExactlyOne(String namedQuery, List<Param> params, Class<T> type) throws PreventEJBLoggingException;

}
