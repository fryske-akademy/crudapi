/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.services;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.fryske_akademy.jpa.EntityInterface;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * Generic interface for write operations on entities
 *
 * @author eduard
 */
public interface CrudWriteService {

    <T> T create(T t);

    <T> T update(T t);

    /**
     * create or update depending on {@link EntityInterface#isTransient() }.
     *
     * @param <T>
     * @param t
     * @return
     */
    <T extends EntityInterface> T save(T t);

    void delete(EntityInterface t);

    /**
     * refresh an entity from the database
     * @param t
     * @param <T>
     * @return
     */
    <T> T refresh(T t);

    /**
     * save multiple entities, NOTE that normally only after commit or flush
     * entitymanagers will reflect changes caused by saving all entities, this
     * may cause entitylisteners to fail. You may need to perform checks on the
     * collection before calling this method. Also note that entitylisteners
     * will execute within the batch transaction, you may need to specify
     * {@link Transactional.TxType} on the bean that you use in your listeners,
     * use isolation level 0 (not recommended) or use XA when you need to access
     * resources from your listeners. When flushCount != null flushes and clears
     * the entitymanager every flushCount entities. NOTE that this method
     * executes in one (possibly big) sql transaction! You can use
     * {@link org.fryske_akademy.jpa.EntityException} in for example your listeners to find out in case
     * of failure which enitity caused it.
     *
     *
     * @param entities
     * @param flushCount when not null try to optimize (flush/clear) every so
     * many entities
     * @return the saved entities
     */
    <T extends EntityInterface> Stream<T> batchSave(Collection<T> entities, Integer flushCount);

    /**
     * save multiple entities, NOTE that normally only after commit or flush
     * entitymanagers will reflect changes caused by saving all entities, this
     * may cause entitylisteners to fail. You may need to perform checks on the
     * collection before calling this method. Also note that entitylisteners
     * will execute within the batch transaction, you may need to specify
     * {@link Transactional.TxType} on the bean that you use in your listeners,
     * use isolation level 0 (not recommended) or use XA when you need to access
     * resources from your listeners. When flushCount != null flushes and clears
     * the entitymanager every flushCount entities. NOTE that this method
     * executes in one (possibly big) sql transaction! You can use
     * {@link org.fryske_akademy.jpa.EntityException} in for example your listeners to find out in case
     * of failure which enitity caused it.
     *
     *
     * @param entities
     * @param flushCount when not null try to optimize (flush/clear) every so
     * many entities
     * @return number of entities deleted
     */
    int batchDelete(Collection<? extends EntityInterface> entities, Integer flushCount);
    
    EntityManager getEntityManager();

}
