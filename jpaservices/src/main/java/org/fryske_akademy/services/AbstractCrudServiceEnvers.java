/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.services;

/*-
 * #%L
 * jpaservices
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jpa.RevInfo;
import org.fryske_akademy.jpa.RevisionInfo;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.hibernate.envers.query.criteria.internal.IdentifierEqAuditExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @author eduard
 */
public abstract class AbstractCrudServiceEnvers extends AbstractCrudService implements Auditing{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCrudServiceEnvers.class.getName());

    /**
     * When more revisions of the same entity are found for this revision, the last is returned
     * @param n
     * @param type
     * @param <T>
     * @return
     */
    @Override
    public <T> T getRevision(Number n, Class<T> type) {
        if (!type.isAnnotationPresent(Audited.class)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("No @Audited annotation on " + type.getName());
            }
            return null;
        }
        return (T) AuditReaderFactory.get(getEntityManager()).createQuery().forEntitiesAtRevision(type, n).getSingleResult();
    }

    @Override
    public <T> List<RevInfo<T>> getRevisionInfo(Serializable id, Integer max, Class<T> type) {
        List<RevInfo<T>> result = new ArrayList<>(5);
        if (!type.isAnnotationPresent(Audited.class)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("No @Audited annotation on " + type.getName());
            }
            return result;
        }
        if (id == null) {
            return result;
        }
        AuditQuery query = AuditReaderFactory.get(getEntityManager()).createQuery().forRevisionsOfEntity(type, false, true).addOrder(AuditEntity.revisionNumber().desc());
        query.add(new IdentifierEqAuditExpression(null, id, true)).setMaxResults((max == null) ? 5 : max);
        List<Object[]> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            return result;
        }
        for (Object[] object : resultList) {
            RevisionType rt = (RevisionType) object[2];
            switch (rt) {
                case ADD -> result.add(new RevInfo<>((T) object[0], RevInfo.TYPE.CREATE, (RevisionInfo) object[1]));
                case MOD -> result.add(new RevInfo<>((T) object[0], RevInfo.TYPE.UPDATE, (RevisionInfo) object[1]));
                case DEL -> result.add(new RevInfo<>((T) object[0], RevInfo.TYPE.DELETE, (RevisionInfo) object[1]));
            }
        }
        return result;
    }

    @Override
    public <T> List<Number> getRevisionNumbers(Serializable id, Class<T> type) {
        if (!type.isAnnotationPresent(Audited.class)) {
            return Collections.emptyList();
        }
        return AuditReaderFactory.get(getEntityManager()).getRevisions(type, id);
    }

    @Override
    public <T extends EntityInterface> T previousState(Serializable id, Class<T> type, int stepsBack) {
        List<RevInfo<T>> revisionInfo = getRevisionInfo(id, stepsBack + 1, type);
        return revisionInfo.size()==2?revisionInfo.get(stepsBack).entity():null;
    }

    @Override
    public <E> List<RevInfo<E>> changes(E e, Integer max) {
        return getRevisionInfo(((EntityInterface)e).getId(), max, e.getClass())
                .stream().map(ri -> new RevInfo<>((E) ri.entity(), ri.type(), ri.revisionInfo()))
                .collect(Collectors.toList());
    }

}

