/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.jsf;

/*-
 * #%L
 * primfacesgui
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.enterprise.inject.spi.CDI;
import jakarta.faces.convert.Converter;
import jakarta.inject.Inject;
import jakarta.security.enterprise.SecurityContext;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jsf.util.JsfUtil;
import org.fryske_akademy.services.CrudReadService;
import org.fryske_akademy.services.CrudWriteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serial;
import java.io.Serializable;

/**
 * Base controller (bean) that is not specialized in a specific entity.
 *
 * @author eduard
 */
public abstract class AbstractController implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractController.class.getName());

    public static final String DEFAULT_BUNDLE_NAME = "/Bundle";

    /**
     * The name of the bundle in which messages are sought, see {@link JsfUtil#getLocaleBundle(String)
     * }.
     *
     * @return
     */
    protected String getBundleName() {
        return DEFAULT_BUNDLE_NAME;
    }

    /**
     * Return a message from the localized {@link #getBundleName()} or the key if it is not found.
     * @param key
     * @return
     */
    protected String getMessage(String key) {
        return JsfUtil.getFromBundle(JsfUtil.getLocaleBundle(getBundleName()),key);
    }
    public enum PersistAction {
        CREATE,
        DELETE,
        UPDATE
    }

    /**
     * NOTE when your bean uses a passivation capable scope (i.e. SessionScoped) the field should be transient and
     * you should have a private readObject in which you initialize CrudReadService, see {@link Util#getBean(java.lang.Class, java.lang.annotation.Annotation...) }.
     * @return 
     */
    public abstract CrudReadService getCrudReadService();
    /**
     * NOTE when bean uses a passivation capable scope (i.e. SessionScoped) the field should be transient and
     * you should have a private readObject in which you initialize CrudWriteService, see {@link Util#getBean(java.lang.Class, java.lang.annotation.Annotation...) }.
     * @return 
     */
    public abstract CrudWriteService getCrudWriteService();

    /**
     * Call {@link #persist(org.fryske_akademy.jpa.EntityInterface, org.fryske_akademy.jsf.AbstractController.PersistAction, java.lang.String) 
     * }
     * with persisAction + " success"
     *
     * @param <T>
     * @param edited
     * @param persistAction
     * @return
     */
    public <T extends EntityInterface> T persist(T edited, PersistAction persistAction) {
        return persist(edited, persistAction, persistAction + " success");
    }

    /**
     * Call {@link #persist(org.fryske_akademy.jpa.EntityInterface, org.fryske_akademy.jsf.AbstractController.PersistAction, java.lang.String, boolean)
     * }
     * with true
     *
     * @param <T>
     * @param edited
     * @param persistAction
     * @param successMessage
     * @return
     */
    public <T extends EntityInterface> T persist(T edited, PersistAction persistAction, String successMessage) {
        return persist(edited, persistAction, successMessage, true);
    }

    /**
     * Calls {@link CrudWriteService#create(Object)} , update or
     * delete, calls {@link JsfUtil#addSuccessMessage(java.lang.String) } with
     * the succesMessage appended. The entity is appended in an understandable way either using
     * a converter or toString. In case of an exception, when callHandle
     * is true, call
     * {@link JsfUtil#handleException(java.lang.RuntimeException, java.lang.String)},
     * otherwise the exception is rethrowm.
     *
     * @param edited
     * @param persistAction
     * @param successMessage the message with the entity id appended
     * @param callHandle when true call {@link JsfUtil#handleExceptionRethrow(java.lang.RuntimeException, java.lang.String)
     * }, otherwise the exception is rethrown.
     * @return the up to date or deleted entity
     */
    public <T extends EntityInterface> T persist(T edited, PersistAction persistAction, String successMessage, boolean callHandle) {
        if (edited != null) {
            Converter converter = JsfUtil.getConverter(edited.getClass());
            try {
                switch (persistAction) {
                    case CREATE:
                        edited = getCrudWriteService().create(edited);
                        break;
                    case UPDATE:
                        edited = getCrudWriteService().update(edited);
                        break;
                    case DELETE:
                        getCrudWriteService().delete(edited);
                        break;
                }

                JsfUtil.addSuccessMessage(successMessage + " " + (converter != null ? converter.getAsString(null, null, edited) : edited));
            } catch (RuntimeException ex) {
                if (callHandle) {
                    // rethrow, so client actions following failed persist won't be executed
                    JsfUtil.handleExceptionRethrow(ex, persistAction + " " + (converter != null ? converter.getAsString(null, null, edited) : edited) + " failed: ");
                } else {
                    throw ex;
                }
            }
        } else {
            LOGGER.warn("entity to persist is null");
        }
        return edited;
    }

    /**
     * for use in primefaces keyboard layoutTemplate
     *
     * @return
     */
    public String diacriticsKeyBoard() {
        return "àáäâÀÁÂÄåÅ-back,èéëêÈÉÊË-clear,ìíïîÌÍÎÏ-close,òóöôÒÓÔÖ,ùúüûÙÚÛÜ,*?";
    }

    
    /**
     * can be appended to an outcome of an action when is a redirect is desired.
     */
    public static final String FACESREDIRECTTRUE = "?faces-redirect=true";

    @Inject
    private transient SecurityContext securityContext;
    
    @Serial
    private void readObject(java.io.ObjectInputStream stream) throws ClassNotFoundException, IOException {
      stream.defaultReadObject();
      securityContext = CDI.current().select(SecurityContext.class).get();
    }    

    protected SecurityContext getSecurityContext() {
        return securityContext;
    }

}
