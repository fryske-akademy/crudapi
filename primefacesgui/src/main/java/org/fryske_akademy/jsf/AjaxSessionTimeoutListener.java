/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fryske_akademy.jsf;

/*-
 * #%L
 * primefacesgui
 * %%
 * Copyright (C) 2018 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.faces.context.FacesContext;
import jakarta.faces.event.PhaseEvent;
import jakarta.faces.event.PhaseId;
import jakarta.faces.event.PhaseListener;
import jakarta.servlet.DispatcherType;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * This class provides the possibility to deal with session timeouts and ajax
 * requests in a user friendly way (instead of not seeing anything).
 * It works in a payara/mojarra environment and perhaps in other environments.
 * Typically you would write a timeout.xhtml in
 * which you inform the user and provide a link or meta refresh to a protected (login)
 * page, override {@link #getTimeoutPath(jakarta.servlet.http.HttpServletRequest)
 * } and declare as phase-listener in faces-config. This is how it works:
 * <ol>
 * <li>ajax request encounters invalid session</li>
 * <li>The server forwards to your login page</li>
 * <li>this listener detects {@link DispatcherType#FORWARD} to {@link #getLoginPath()
 * }</li>
 * <li>{@link #handleAjaxAfterTimeout(jakarta.faces.context.FacesContext, jakarta.servlet.http.HttpServletRequest)
 * } is called</li>
 * <li>In this method a redirect is done to {@link #getTimeoutPath(jakarta.servlet.http.HttpServletRequest) },
 * which should be a <strong>protected</strong> page, possibly via a non-protected page using
 * meta refresh and showing some friendly message. If you directly redirect to the login page the original request may be reposted
 * (depending on environment) and cause a partial ajax response with a ViewExpiredException directly in the browser window.</li>
 * <li>your security setup redirects to the login page</li>
 * <li>after login you land on the page from step 5</li>
 * </ol>
 * declare this phase-listener in your faces-config
 *
 * @author eduard
 */
public class AjaxSessionTimeoutListener implements PhaseListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AjaxSessionTimeoutListener.class.getName());

    public static final String LOGINPATH = "/login.xhtml";

    /**
     * @return {@link #LOGINPATH}
     */
    protected String getLoginPath() {
        return LOGINPATH;
    }

    /**
     * If this is a {@link DispatcherType#FORWARD} to {@link #getLoginPath() }
     * and the response hasn't been rendered yet, call {@link #handleAjaxAfterTimeout(jakarta.faces.context.FacesContext, jakarta.servlet.http.HttpServletRequest)
     * }.
     *
     * @param event
     */
    @Override
    public void beforePhase(final PhaseEvent event) {
        final FacesContext facesContext = event.getFacesContext();
        if (!facesContext.getPartialViewContext().isAjaxRequest() || facesContext.getRenderResponse()) {
            if (facesContext.getRenderResponse() && LOGGER.isDebugEnabled()) {
                final HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
                LOGGER.debug(String.format("response rendered, not handling %s", request.getServletPath()));
            }
            return;
        }

        final HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        if (request.getDispatcherType() == DispatcherType.FORWARD &&
                getLoginPath().equals(request.getServletPath())) {
            // isLoginRedirection()
            try {
                handleAjaxAfterTimeout(facesContext, request);
            } catch (final IOException e) {
                LOGGER.error( "unable to redirect", e);
            }
        }
    }

    /**
     * redirect to {@link #getTimeoutPath(jakarta.servlet.http.HttpServletRequest)
     * }.
     *
     * @param facesContext
     * @param request the original ajax request
     * @throws java.io.IOException
     */
    protected void handleAjaxAfterTimeout(FacesContext facesContext, HttpServletRequest request) throws IOException {
        facesContext.getExternalContext().redirect(getTimeoutPath(request));
    }

    /**
     * This implementation returns to the context path root, you may choose to
     * override and redirect to a "timeout.xhtml" where you provide users some
     * info and options
     *
     * @return
     */
    protected String getTimeoutPath(HttpServletRequest request) {
        return request.getContextPath();
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
}
