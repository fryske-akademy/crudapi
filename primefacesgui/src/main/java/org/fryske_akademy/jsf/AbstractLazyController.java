/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.jsf;

/*-
 * #%L
 * primfacesgui
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.ClassHelper;
import jakarta.annotation.PostConstruct;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.jsf.lazy.AbstractLazyModel;
import org.fryske_akademy.services.CrudReadService;

import java.util.List;

/**
 * Baseclass for controllers supporting lazy loading, this class needs an accompanying {@link AbstractLazyModel} of the same scope and the other way around. Filtering support is in
 * {@link AbstractLazyModel}.
 *
 * @author eduard
 */
public abstract class AbstractLazyController<E extends EntityInterface, M extends AbstractLazyModel<E>> extends AbstractEntityController<E> {

    private M lazyModel;

    @PostConstruct
    private void wiring() {
        Class c = ClassHelper.findParameterClass(1,getClass(),AbstractLazyController.class);
        char first = c.getSimpleName().charAt(0);
        lazyModel = (M) Util.getBean(c,Character.toLowerCase(first)+c.getSimpleName().substring(1));
        lazyModel.setLazyController( this);
    }

    public M getLazyModel() {
        return lazyModel;
    }

    @Override
    public Filtering<E> getFiltering() {
        return lazyModel;
    }

    /**
     * implement this to auto complete user input, see {@link CrudReadService#find(java.lang.String, java.util.List, java.lang.Integer, java.lang.Integer, java.lang.Class)
     * }
     *
     * @param query
     * @return
     */
    public abstract List<String> complete(String query);

    /**
     * when true use or when several parameters are given
     *
     * @see Param#getAndOr()
     * @return
     */
    public boolean isUseOr() {
        return lazyModel.isUseOr();
    }

    public void setUseOr(boolean useOr) {
        lazyModel.setUseOr(useOr);
    }

    public boolean isCaseInsensitive() {
        return lazyModel.isCaseInsensitive();
    }

    public void setCaseInsensitive(boolean caseInsensitive) {
        lazyModel.setCaseInsensitive(caseInsensitive);
    }

    /**
     * when true support syntax in parameter value
     *
     * @see Param.Builder
     * @return
     */
    public boolean isSyntaxInValue() {
        return lazyModel.isSyntaxInValue();
    }

    public void setSyntaxInvalue(boolean syntaxInvalue) {
        lazyModel.setSyntaxInValue(syntaxInvalue);
    }

    /**
     * After calling the super, removes
     * the entity from the wrapped data which makes the gui datatable show
     * correct data.
     *
     * @param entity
     */
    @Override
    public void destroy(E entity) {
        super.destroy(entity);
        lazyModel.getWrappedData().remove(entity);
    }

}
