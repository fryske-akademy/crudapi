/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fryske_akademy.jsf;

/*-
 * #%L
 * primefacesgui
 * %%
 * Copyright (C) 2018 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jsf.lazy.AbstractLazyModel;
import org.fryske_akademy.jsf.lazy.NewSupportingLazyModel;
import org.primefaces.PrimeFaces;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

/**
 * Controller that supports adding new entities via a row in a datatable.
 * @author eduard
 */
public abstract class NewSupportingLazyController<E extends EntityInterface, M extends NewSupportingLazyModel<E>> extends AbstractLazyController<E, M>{

    
    /**
     * javascript to execute when new row is on last page in datatable, called from {@link #editNew(java.lang.String) }
     * @param table
     * @return 
     */
    protected abstract String newRowOnLastPage(String table);
    /**
     * javascript to execute when new row is on current page in datatable, called from {@link #editNew(java.lang.String) }
     * @param table
     * @return 
     */
    protected abstract String newRowOnCurrentPage(String table);

    /**
     * Calls {@link #prepareCreate() } and {@link #executOnClient(java.lang.String) }.
     * @param table
     * @throws InstantiationException
     * @throws IllegalAccessException 
     */
    public void editNew(String table) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        prepareCreate();
        executOnClient(table);
    }

    /**
     * Executes {@link #newRowOnCurrentPage(java.lang.String) } or
     * {@link #newRowOnLastPage(java.lang.String) } on the client.
     * @param table 
     */
    protected void executOnClient(String table) {
        if (newOnLastPage()) {
            PrimeFaces.current().executeScript(newRowOnLastPage(table));
        } else {
            PrimeFaces.current().executeScript(newRowOnCurrentPage(table));
        }
    }

    /**
     * Calls {@link #copy() } and {@link #executOnClient(java.lang.String) }.
     * @param table
     * @throws InstantiationException
     * @throws IllegalAccessException 
     */
    public void copy(String table) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        copy();
        executOnClient(table);
    }

    /**
     * returns true when {@link AbstractLazyModel#getPageSize() } &lt;= {@link AbstractLazyModel#getWrappedData() wrapped datasize}.
     * In this case the new entity, added in {@link NewSupportingLazyModel#setWrappedData(java.lang.Object) }, will land on the last page.
     * @return 
     */
    protected boolean newOnLastPage() {
        if (getLazyModel().getWrappedData() != null) {
            // new entity lands on last page
            return getLazyModel().getPageSize() <= getLazyModel().getWrappedData().size();
        }
        return false;
    }

    /**
     * calls the super if no newEntity is present
     * @throws InstantiationException
     * @throws IllegalAccessException 
     */
    @Override
    public void prepareCreate() throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if (getNewEntity() == null) {
            super.prepareCreate();
        }
    }
    
    /**
     * return id or -1 for new entity, for the new entity the rowKey will be -1 which makes it visible in a datatable. Call this method from the rowkey attribute in your datatable. NOTE that it isn't necessary
     * to use the rowkey attribute, because of {@link AbstractLazyModel#getRowKey(EntityInterface)}.
     *
     * @param id
     * @return
     */
    public Serializable getRowKey(Serializable id) {
        return id != null ? id : -1;
    }

    /**
     * calls the super and {@link #setNewEntity(org.fryske_akademy.jpa.EntityInterface) } with null.
     * @param e
     * @return
     * @throws Exception 
     */
    @Override
    public E create(E e) {
        E create = super.create(e);
        setNewEntity(null);
        return create;
    }

}
