/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.jsf;

/*-
 * #%L
 * primfacesgui
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.fryske_akademy.jpa.EntityInterface;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Generic filtering support to use with primefaces datatable
 * @author eduard
 */
public interface Filtering<E extends EntityInterface> extends Serializable {

    /**
     * Use this in primefaces EL expression for filteredValue
     * @return
     */
    List<E> getFiltered();

    void setFiltered(List<E> filtered);

    /**
     * used this in primefaces EL expression for filterValue: #{controller.filters['filtername']}
     * @return
     */
    Map<String, Object> getFilters();
    
    Filtering<E> add(String key, Object value);
    
    boolean hasFilter(String key);
    
    /**
     * empty the filters
     */
    Filtering<E> clear();
    
}
