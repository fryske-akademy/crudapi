package org.fryske_akademy.jsf.util;

/*-
 * #%L
 * primfacesgui
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import org.fryske_akademy.Util;
import org.fryske_akademy.jsf.lazy.AbstractLazyModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class JsfUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsfUtil.class.getName());

    /**
     * Same as {@link #handleException(RuntimeException, String)} and rethrows the exception
     *
     * @param ex
     * @param prefix
     */
    public static void handleExceptionRethrow(RuntimeException ex, String prefix) {
        handleException(ex,prefix,true);
    }

    /**
     * Calls {@link FacesContext#validationFailed() }, {@link Util#deepestCause(java.lang.Throwable)
     * } and {@link #addErrorMessage(java.lang.String) } with the prefix and the
     * localized message from the deepest exception. When the deepestCause is a ConstraintViolationException
     * {@link Util#formatConstraintException(ConstraintViolationException)} is called.
     *
     * @param ex
     * @param prefix
     */
    public static void handleException(RuntimeException ex, String prefix) {
        handleException(ex,prefix,false);
    }

    private static void handleException(RuntimeException ex, String prefix, boolean rethrow) throws RuntimeException {
        FacesContext.getCurrentInstance().validationFailed();
        Throwable t = Util.deepestCause(ex);
        String msg = t instanceof ConstraintViolationException cve ?
                Util.formatConstraintException(cve).toString() : t.getLocalizedMessage();
        addErrorMessage(prefix + msg);
        if (rethrow) throw ex;
    }

    public static boolean isValidationFailed() {
        return FacesContext.getCurrentInstance().isValidationFailed();
    }

    public static void addErrorMessage(Exception ex, String defaultMsg) {
        String msg = ex.getLocalizedMessage();
        if (msg != null && !msg.isEmpty()) {
            addErrorMessage(msg);
        } else {
            addErrorMessage(defaultMsg);
        }
    }

    public static void addErrorMessages(List<String> messages) {
        for (String message : messages) {
            addErrorMessage(message);
        }
    }

    /**
     * uses null as clientId
     *
     * @param msg
     */
    public static void addErrorMessage(String msg) {
        String cid = null;
        addErrorMessage(cid, msg);
    }

    public static void addErrorMessage(String clientId, String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
        FacesContext.getCurrentInstance().addMessage(clientId, facesMsg);
    }

    /**
     * Uses successInfo as client id
     *
     * @param msg
     */
    public static void addSuccessMessage(String msg) {
        addSuccessMessage("successInfo", msg);
    }

    public static void addSuccessMessage(String clientId, String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
        FacesContext.getCurrentInstance().addMessage(clientId, facesMsg);
    }

    /**
     * evaluate EL expression and return an instance of clazz
     * @param expression
     * @param clazz
     * @return
     * @param <T>
     */
    public static <T> T findOrCreateInContext(String expression, Class<T> clazz) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getApplication().evaluateExpressionGet(facesContext, expression, clazz);
    }

    private static final Map<Class,Converter> CLASS_CONVERTER_MAP = new HashMap<>();

    /**
     * @deprecated preferrably inject converters where needed
     * uses static cache
     * @param clazz
     * @return
     */
    public static Converter getConverter(Class clazz) {
        if (CLASS_CONVERTER_MAP.containsKey(clazz)) {
            return CLASS_CONVERTER_MAP.get(clazz);
        }
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Converter converter = facesContext.getApplication().createConverter(clazz);
        if (converter==null) {
            LOGGER.warn(String.format("no converter found for %s",clazz));
        } else {
            CLASS_CONVERTER_MAP.put(clazz,converter);
        }
        return CLASS_CONVERTER_MAP.get(clazz);
    }

    /**
     * context path plus servlet path
     *
     * @return
     */
    public static String fullServletPath() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        return externalContext.getRequestContextPath() + externalContext.getRequestServletPath();
    }

    public static String contextPath() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        return externalContext.getRequestContextPath();
    }

    public static void redirectToServletPath(String servletPath) throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(externalContext.getRequestContextPath() + servletPath);
    }

    public static void logout() throws ServletException {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).logout();
    }

    private static final ResourceBundle rootBundle = ResourceBundle.getBundle("EMPTY", Locale.ROOT);

    /**
     * return a resource for the current locale (from faces context or default
     * locale), or an empty bundle. Using this method prevents
     * MissingResourceException and logs the bundle (property file) that isn't
     * found.
     *
     * @param bundle
     * @return
     */
    public static ResourceBundle getLocaleBundle(String bundle) {
        FacesContext currentInstance = FacesContext.getCurrentInstance();
        Locale loc = currentInstance != null ? currentInstance.getViewRoot().getLocale() : Locale.getDefault();
        try {
            return ResourceBundle.getBundle(bundle, loc);
        } catch (MissingResourceException e) {
            LOGGER.warn("resource (properties file) not found: " + bundle + " for locale: " + loc + ", returning EMPTY");
            return rootBundle;
        }

    }

    /**
     * Return the message from the bundle or the key when it's not found. Keys not found are logged.
     * @param bundle
     * @param key
     * @return
     */
    public static String getFromBundle(ResourceBundle bundle, String key) {
        if (bundle.containsKey(key)) {
            return bundle.getString(key);
        } else {
            LOGGER.warn(String.format("%s not found in language bundle %s / %s", key, bundle.getBaseBundleName(),bundle.getLocale()));
            return "???"+key+"???";
        }
    }

    /**
     * Calls {@link Util#wrapStringInWildCards(String, boolean)}
     * @see AbstractLazyModel#isSyntaxInValue()
     *
     */
    public static String wrapStringInWildCards(String value, boolean syntaxInValue) {
        return Util.wrapStringInWildCards(value, syntaxInValue);
    }
}
