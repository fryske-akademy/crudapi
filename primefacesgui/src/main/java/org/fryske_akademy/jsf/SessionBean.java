package org.fryske_akademy.jsf;

/*-
 * #%L
 * primfacesgui
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ValueChangeEvent;
import jakarta.inject.Inject;
import jakarta.security.enterprise.SecurityContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.fryske_akademy.jsf.util.CookieHelper;
import org.fryske_akademy.jsf.util.JsfUtil;
import org.fryske_akademy.services.AbstractCrudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serial;
import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Manages logout, internationalization and theming. If you override this and use @Named("sessionBean"), 
 * you can for example use it in web.xml for primefaces theming:
 * <pre>
 &lt;context-param>
    &lt;param-name>primefaces.THEME&lt;/param-name>
    &lt;param-value>#{sessionBean.currentTheme}&lt;/param-value>
 &lt;/context-param>
 * </pre>.
 * You can add themes and/or languages and set defaults in a @Postconstruct method.
 *
 * @author eduard
 */
public abstract class SessionBean implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SessionBean.class);

    public static final String LANGUAGE_COOKIE = "language";
    public static final String THEME_COOKIE = "theme";

    private String language;
    private Locale locale;

    private static final Map<String, Locale> languageLocales = new LinkedHashMap<>(3);
    private static final List<String> languages = new ArrayList<>(3);
    private final List<String> themes = new ArrayList<>(40);
    private String currentTheme;

    public SessionBean addTheme(String theme) {
        if (!themes.contains(theme)) {
            themes.add(theme);
        }
        return this;
    }

    public SessionBean removeTheme(String name) {
        themes.remove(name);
        return this;
    }

    @Inject
    private transient SecurityContext securityContext;

    @Serial
    private void readObject(java.io.ObjectInputStream stream) throws ClassNotFoundException, IOException {
        stream.defaultReadObject();
        securityContext = CDI.current().select(SecurityContext.class).get();
    }

    @PostConstruct
    private void init() {
        addTheme("nova-light")
        .addTheme("nova-dark")
        .addTheme("nova-colored")
        .addTheme("luna-blue")
        .addTheme("luna-amber")
        .addTheme("luna-green")
        .addTheme("luna-pink")
        .addTheme("arya")
        .addTheme("saga")
        .addTheme("vela");
        currentTheme = "saga";
        initLanguages();
        if (!languageLocales.isEmpty()) {
            setLanguage(languageLocales.entrySet().stream().findFirst().get().getKey());
        }
        initFromCookies();
    }

    /**
     * list available themes. You can add themes you bought by calling
     * {@link #addTheme(java.lang.String) } from for example the constructor of
     * a subclass.
     *
     * @return
     */
    public List<String> getThemes() {
        return themes;
    }

    public String getCurrentTheme() {
        return currentTheme;
    }

    public void setCurrentTheme(String currentTheme) {
        if (!themes.contains(currentTheme)) {
            throw new IllegalArgumentException(currentTheme + " not supported");
        }
        this.currentTheme = currentTheme;
    }

    public void themeChanged(ValueChangeEvent event) {
        setCurrentTheme((String) event.getNewValue());
        setCookie(THEME_COOKIE, currentTheme);
    }

    public SessionBean addLanguage(String name, String code, String region) {
        languageLocales.put(name, new Locale(code, region));
        if (!languages.contains(name)) {
            languages.add(name);
        }
        return this;
    }

    @Deprecated
    protected static void setCookie(String name, String value) {
        FacesContext context = FacesContext.getCurrentInstance();
        CookieHelper.replaceCookie(name, value, 60 * 60 * 24 * 365,
                (HttpServletRequest) context.getExternalContext().getRequest(),
                (HttpServletResponse) context.getExternalContext().getResponse());
    }

    @Deprecated
    protected static String getCookie(String name) {
        FacesContext context = FacesContext.getCurrentInstance();
        return CookieHelper.getCookie(name, (HttpServletRequest) context.getExternalContext().getRequest());
    }

    /**
     * Called from @Postconstruct, sets current language and theme from a cookie
     * or to a default
     *
     * @see CookieHelper
     * @deprecated use web storage instead
     */
    @Deprecated
    protected void initFromCookies() {
        try {
            String cookie = getCookie(LANGUAGE_COOKIE);
            setLanguage(cookie != null ? cookie : "Dutch");
            cookie = getCookie(THEME_COOKIE);
            setCurrentTheme(cookie != null ? cookie : "nova-light");
        } catch (Exception e) {
            LOGGER.warn("problem setting language or theme", e);
        }
    }

    /**
     * Called from @Postconstruct, initializes frisian, ducth and english.
     * Override this to support more / other languages
     */
    protected void initLanguages() {
        synchronized (languageLocales) {
            if (languageLocales.isEmpty()) {
                addLanguage("Dutch", "nl", "NL");
                addLanguage("English", "en", "EN");
                addLanguage("Frisian", "fy", "NL");
            }
        }
    }

    /**
     * use this for user choice (dropdown)
     *
     * @return
     */
    public List<String> getLanguages() {
        return languages;
    }

    /**
     * The current choice
     *
     * @return
     */
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        if (languageLocales.containsKey(language)) {
            locale = languageLocales.get(language);
            FacesContext.getCurrentInstance()
                    .getViewRoot().setLocale(locale);
            this.language = language;
        } else {
            throw new IllegalArgumentException(language + " not supported");
        }
    }

    /**
     * the current locale, use this in @locale in f:view
     *
     * @return
     */
    public Locale getLocale() {
        return locale;
    }

    private final Map<String, Boolean> allowed = new HashMap<>(10);

    /**
     * caches access in the session, login/logout needed after changes
     *
     * @param role
     * @return
     */
    public boolean mayEdit(String role) {
        synchronized (allowed) {
            if (allowed.containsKey(role)) {
                return allowed.get(role);
            } else {
                allowed.put(role, securityContext.isCallerInRole(role));
            }
        }
        return allowed.get(role);
    }

    /**
     * Calls {@link #mayEdit(java.lang.String) } with {@link AbstractCrudService#EDITORROLE}.
     *
     * @return
     */
    public boolean mayEdit() {
        return mayEdit(AbstractCrudService.EDITORROLE);
    }

    /**
     * Call this when user chooses other language
     *
     * @param e
     */
    public void languageChanged(ValueChangeEvent e) {
        setLanguage((String) e.getNewValue());
        setCookie(LANGUAGE_COOKIE, language);
    }

    private String user;

    public String getUser() {
        if (user != null) {
            return user;
        }
        Principal p = securityContext.getCallerPrincipal();
        user = (p == null) ? "not logged in" : p.getName();
        return user;
    }

    /**
     * ises {@link #getLogoutPath() }
     */
    public void logout() {
        try {
            JsfUtil.logout();
            FacesContext.getCurrentInstance().getExternalContext().redirect(getLogoutPath());
        } catch (ServletException | IOException ex) {
            LoggerFactory.getLogger(SessionBean.class.getName()).error(null, ex);
        }
    }

    /**
     * @return contextPath + "/login.xhtml"
     */
    protected static String getLogoutPath() {
        return JsfUtil.contextPath() + "/login.xhtml";
    }

}
