/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.jsf;

/*-
 * #%L
 * primfacesgui
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.ClassHelper;
import jakarta.annotation.PostConstruct;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.model.DataModel;
import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jpa.RevInfo;
import org.fryske_akademy.jsf.util.JsfUtil;
import org.fryske_akademy.services.Auditing;
import org.hibernate.envers.RevisionType;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * baseclass for controllers, it supports selecting, copying, filtering, crud
 * and showing audit information. The methods doing crud, based on a RowEditEvent,
 * keep the datatable row up to date (not completely guaranteed by primefaces).
 *
 * @author eduard
 */
public abstract class AbstractEntityController<E extends EntityInterface> extends AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractEntityController.class.getName());

    private E selected;
    private E newEntity;

    private Class<E> clazz;

    private boolean rememberTableState = true;

    public Class<E> getClazz() {
        return clazz;
    }

    @PostConstruct
    private void init() {
        clazz = (Class<E>) ClassHelper.findParameterClass(0, getClass(), AbstractEntityController.class);
    }

    /**
     * Remember sorting, filtering, column order, selection, column widths, page
     * number (@multiViewState). When {@link #useFilterMechanism()} is true call !{@link #isFiltering()}.
     *
     * @return
     */
    public boolean isRememberTableState() {
        return rememberTableState && (!useFilterMechanism() || !isFiltering());
    }

    /**
     * @return true if you want to use the mechanism to temporarily disable remembering table state based on a request parameter "state=filtering".
     */
    protected abstract boolean useFilterMechanism();

    /**
     * This method returns true when a request parameter "state" with value "filtering" is
     * found.
     *
     * @return
     */
    protected boolean isFiltering() {
        return FILTERING.equals(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(STATE));
    }

    public void setRememberTableState(boolean rememberTableState) {
        this.rememberTableState = rememberTableState;
    }

    public E getSelected() {
        return selected;
    }

    public void setSelected(E selected) {
        this.selected = selected;
    }

    public E getNewEntity() {
        return newEntity;
    }

    public void setNewEntity(E newEntity) {
        this.newEntity = newEntity;
    }

    public void onRowSelect(SelectEvent event) {
        if (event != null) {
            setSelected((E) event.getObject());
        }
    }

    /**
     * Instantiates a new entity and calls {@link #fillNew(org.fryske_akademy.jpa.EntityInterface)
     * }
     *
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void prepareCreate() throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        newEntity = clazz.getDeclaredConstructor().newInstance();
        fillNew(newEntity);
    }

    /**
     * calls {@link #create(org.fryske_akademy.jpa.EntityInterface) } with the new entity from this controller.
     *
     * @return
     */
    public E create() {
        return create(newEntity);
    }

    /**
     * calls {@link #persist(org.fryske_akademy.jpa.EntityInterface, org.fryske_akademy.jsf.AbstractController.PersistAction, java.lang.String)
     * }. Uses property "Created" from bundle as message.
     *
     * @param e
     * @return
     */
    public E create(E e) {
        return persist(e, PersistAction.CREATE, JsfUtil.getFromBundle(JsfUtil.getLocaleBundle(getBundleName()), "Created"));
    }

    /**
     * calls {@link #create(org.fryske_akademy.jpa.EntityInterface) } and
     * {@link #refreshRow(org.primefaces.event.RowEditEvent, org.fryske_akademy.jpa.EntityInterface, boolean) }. Uses
     * property "Created" from bundle as message.
     *
     * @param event
     * @return
     */
    public E create(RowEditEvent event) {
        E e = create((E) event.getObject());
        refreshRow(event, e, true);
        return e;
    }

    /**
     * list all entities, use with care!
     *
     * @return
     */
    public List<E> listAll() {
        return getCrudReadService().findAll(clazz);
    }

    public abstract Filtering<E> getFiltering();

    /**
     * return the value of a filter.
     *
     * @param key
     * @return
     */
    public Object getFilterValue(String key) {
        return getFiltering().getFilters().get(key);
    }

    public String getFilterString(String key) {
        Object value = getFiltering().getFilters().get(key);
        return value == null ? null : String.valueOf(value);
    }

    /**
     * active filters on a page, use this as primefaces @filterValue:
     * filterValue="#{ctlr.filters['id']}"
     *
     * @return
     */
    public Map<String, Object> getFilters() {
        return getFiltering().getFilters();
    }

    /**
     * Navigate (i.e. in a datatable) to the page containing the argument
     * entity. Implementors can call Filtering.clear().add(java.lang.String,
     * java.lang.Object) and then for example filter, or redirect.
     *
     * @param entity
     * @return the action to perform
     * @see #filterAndRedirect(Filtering, String, String, String)
     */
    public abstract String gotoPageContaining(E entity);

    /**
     * implementors can fill the new object with values from selected, called
     * from {@link #copy() }.
     *
     * @param newEntity
     * @param selected
     */
    protected abstract void fillCopy(E newEntity, E selected);

    /**
     * empty method, called from {@link #prepareCreate() }
     *
     * @param entity
     */
    protected void fillNew(E entity) {
    }

    /**
     * copy a selected entity, call {@link #fillCopy(org.fryske_akademy.jpa.EntityInterface, org.fryske_akademy.jpa.EntityInterface)
     * }, set newEntity to the copy.
     *
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void copy() throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        if (selected != null) {
            E l = clazz.getDeclaredConstructor().newInstance();
            fillCopy(l, selected);
            newEntity = l;
        }
    }

    /**
     * Calls {@link #update(org.fryske_akademy.jpa.EntityInterface) }
     * and {@link #refreshRow(org.primefaces.event.RowEditEvent, org.fryske_akademy.jpa.EntityInterface, boolean)
     * } with the up to date entity and true. Uses property "Updated" from
     * bundle as message.
     *
     * @param editEvent
     * @return the updated entity
     */
    public E update(RowEditEvent editEvent) {
        E t = update((E) editEvent.getObject());
        refreshRow(editEvent, t, true);
        if (t.equals(selected)) selected = t;
        return t;
    }

    /**
     * calls {@link #update(org.fryske_akademy.jpa.EntityInterface) } with the selected entity.
     *
     * @return
     */
    public E update() {
        selected = update(selected);
        return selected;
    }

    /**
     * calls {@link #persist(org.fryske_akademy.jpa.EntityInterface, org.fryske_akademy.jsf.AbstractController.PersistAction, java.lang.String)
     * }. Uses property "Updated" from bundle as message.
     *
     * @param e
     * @return
     */
    public E update(E e) {
        return persist(e, PersistAction.UPDATE, JsfUtil.getFromBundle(JsfUtil.getLocaleBundle(getBundleName()), "Updated"));
    }

    /**
     * {@link #create(org.primefaces.event.RowEditEvent) Create} or
     * {@link #update(org.primefaces.event.RowEditEvent) update} an entity based
     * on {@link EntityInterface#isTransient() }.
     *
     * @param event
     * @return the saved entity
     */
    public E save(RowEditEvent event) {
        E t = (E) event.getObject();
        return t.isTransient() ? create(event) : update(event);
    }

    /**
     * Attempts to show data in sync with the database after create/update or
     * cancel edit. This implementation works for DataTables with a List or a
     * DataModel wrapping a List as value.
     *
     * @param editEvent
     * @param e
     * @param update    true for updates
     */
    protected void refreshRow(RowEditEvent editEvent, EntityInterface e, boolean update) {
        if (editEvent.getComponent() instanceof DataTable dt && e != null && e.getId() != null) {
            Object value = dt.getValue();
            List rows = value instanceof List l ? l : null;
            if (value instanceof DataModel d && d.getWrappedData() instanceof List l) {
                rows = new ArrayList(l);
            }
            if (rows != null && !rows.isEmpty() && dt.getRowIndex() > -1) {
                int rowIndex = value instanceof DataModel d ? d.getRowIndex() : dt.getRowIndex();
                if (update) {
                    rows.set(rowIndex, e);
                } else {
                    rows.set(rowIndex, getCrudReadService().find(e.getId(), getClazz()));
                }
                ((DataModel) value).setWrappedData(Collections.unmodifiableList(rows));
            } else {
                LOGGER.warn(String.format("Cannot refresh datatable row: %s", dt.getRowIndex()));
            }
        } else {
            LOGGER.warn(String.format("Cannot refresh datatable row for: %s", e));
        }
    }

    /**
     * When the objects is in the database {@link #refreshRow(org.primefaces.event.RowEditEvent, org.fryske_akademy.jpa.EntityInterface, boolean)
     * } is called with the canceled entity from the event and false, otherwise {@link #destroy(EntityInterface)} is called.
     *
     * @param editEvent
     */
    public void cancelRow(RowEditEvent editEvent) {
        E e = (E) editEvent.getObject();
        if (e != null && e.getId() == null) {
            LOGGER.warn("canceling a transient entity, calling destroy");
            destroy(e);
            return;
        }
        refreshRow(editEvent, e, false);
    }


    /**
     * Deletes the given entity, which should either be the selected entity or
     * the new entity. Uses property "Deleted" from bundle. When applicable sets
     * selected or new entity to null.
     *
     * @param e
     */
    public void destroy(E e) {
        if (e == null) {
            LOGGER.warn("entity to destroy is null");
            return;
        }
        if (e.isTransient() || e.equals(newEntity)) {
            setNewEntity(null);
        } else {
            persist(e, PersistAction.DELETE, JsfUtil.getFromBundle(JsfUtil.getLocaleBundle(getBundleName()), "Deleted"));
        }
        if (e.equals(selected)) {
            setSelected(null);
        }
    }

    /**
     * return newEntity() when it isn't null and selected is null, otherwise return selected (that may also be the new entity). This method together
     * with {@link #destroy()}, {@link #deleteTitle()} and {@link #disableDelete()} offers a consistent way to present
     * delete functionality to users, it is best to use these methods together. The methods are especially useful if you have a single select table
     * and one delete button, inline delete buttons should work with an argument.
     *
     * @return
     */
    public E toDelete() {
        return (newEntity != null && selected == null) ? newEntity
                : selected;
    }

    /**
     * Call {@link #destroy(org.fryske_akademy.jpa.EntityInterface) } with
     * {@link #toDelete() }.
     */
    public void destroy() {
        destroy(toDelete());
    }

    /**
     * delete should be disabled when {@link #toDelete() } is null;
     *
     * @return
     */
    public boolean disableDelete() {
        return toDelete() == null;
    }

    /**
     * Call {@link #deleteTitle(EntityInterface)} with {@link #toDelete()}
     *
     * @return
     */
    public String deleteTitle() {
        return deleteTitle(toDelete());
    }

    /**
     * show some useful info to a user what is about to be deleted, tries a
     * converter for the entity class to delete.
     *
     * @return
     */
    public String deleteTitle(E entity) {
        if (entity == null) return "";
        Converter converter = JsfUtil.getConverter(entity.getClass());
        return JsfUtil.getFromBundle(JsfUtil.getLocaleBundle(getBundleName()), "Delete") + " "
                + (converter == null ? entity : converter.getAsString(null, null, entity)) + "?";
    }

    /**
     * @return
     * @throws IllegalStateException when {@link #getCrudReadService() } is not
     *                               an instance of Auditing
     */
    protected Auditing getAuditing() {
        if (getCrudReadService() instanceof Auditing a) {
            return a;
        } else {
            throw new IllegalStateException("getCrudReadService() does not return an instance of Auditing");
        }
    }

    /**
     * returns username, revision date and {@link RevisionType} for the last
     * revision found
     *
     * @param entity
     * @return
     * @see Auditing#getRevisionInfo(java.io.Serializable, java.lang.Integer,
     * java.lang.Class)
     */
    public String getLastChangedInfo(E entity) {
        List<RevInfo<E>> revisions = getAuditing().getRevisionInfo(entity.getId(), 1, clazz);
        if (revisions.isEmpty()) {
            return "no changes";
        }
        RevInfo<E> rev = revisions.get(0);
        return rev.revisionInfo().getUsername() + " at " + rev.revisionInfo().getRevisionDate() + " (" + rev.type() + ")";
    }

    public List<RevInfo<E>> getLastChanged(E entity, int max) {
        return getAuditing().getRevisionInfo(entity, max, clazz);
    }

    /**
     * @param controller
     * @param key
     * @param value
     * @param action
     * @return
     * @deprecated use {@link #filterAndRedirect(Filtering, String, String, String)}
     */
    @Deprecated(forRemoval = true)
    protected String filterAndRedirect(AbstractEntityController controller, String key, String value, String action) {
        return filterAndRedirect(controller.getFiltering(), key, value, action);
    }

    /**
     * Clears all filters, adds the key/value to the filters and returns your action with
     * {@link #FACESREDIRECTTRUE} and "state=filtering" appended. This state parameter can temporarily disable remembering table state, otherwise multiViewState will overwrite the filter you just set.
     * To make this work override {@link #useFilterMechanism()}, use {@link #isRememberTableState() } in your EL expression for
     * multiViewState and call PF(datatable).filter() in javascript for example like this:
     * <pre>
     * &lt;c:if test="#{param['state'] == 'filtering'}">
     * &lt;script type="application/javascript">
     * $(document).ready(function () { PF('datalist').filter(); });
     * &lt;/script>
     * &lt;/c:if>
     * </pre>
     *
     * @param filtering
     * @param key
     * @param value
     * @param action
     * @return
     */
    protected String filterAndRedirect(Filtering filtering, String key, String value, String action) {
        filtering.clear().add(key, value);
        // redirect
        return action + FACESREDIRECTTRUE + "&" + STATE + "=" + FILTERING;
    }

    /**
     * will be appended as param key in {@link #filterAndRedirect(Filtering, String, String, String)
     * }
     */
    protected static final String STATE = "state";

    /**
     * will be appended as param value in {@link #filterAndRedirect(Filtering, String, String, String)
     * }
     */
    protected static final String FILTERING = "filtering";

}
