/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.jsf.lazy;

/*-
 * #%L
 * primfacesgui
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.ClassHelper;
import com.vectorprint.StringConverter;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.inject.Inject;
import jakarta.security.enterprise.SecurityContext;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.jpa.Param.Builder;
import org.fryske_akademy.jsf.AbstractLazyController;
import org.fryske_akademy.jsf.Filtering;
import org.fryske_akademy.services.CrudReadService;
import org.fryske_akademy.services.CrudReadService.SORTORDER;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;

import java.io.IOException;
import java.io.Serial;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to support lazy loading with filtering and sorting.
 *
 * @author eduard
 */
public abstract class AbstractLazyModel<E extends EntityInterface> extends LazyDataModel<E> implements Filtering<E> {

    private Class<E> clazz;
    private boolean useOr;
    private boolean syntaxInValue = true;
    private boolean caseInsensitive = false;

    private AbstractLazyController abstractLazyController;

    public AbstractLazyController getLazyController() {
        return abstractLazyController;
    }

    @PostConstruct
    private void init() {
        clazz = (Class<E>) ClassHelper.findParameterClass(0,getClass(), AbstractLazyModel.class);
    }

    /**
     * Called in @Postconstruct of {@link AbstractLazyController}
     *
     * @param lazyController
     */
    public void setLazyController(AbstractLazyController lazyController) {
        if (abstractLazyController == null) {
            abstractLazyController = lazyController;
        } else {
            if (!this.abstractLazyController.equals(lazyController))
                throw new IllegalStateException(String.format("replacing %s with %s not allowed", abstractLazyController, lazyController));
        }
    }

    /**
     * NOTE when your bean uses a passivation capable scope (i.e. SessionScoped)
     * the field should be transient, and you should have a private readObject in
     * which you initialize CrudReadService, see {@link CDI#select(Class, Annotation...)}.
     *
     * @return
     */
    public abstract CrudReadService getCrudReadService();

    /**
     * map holding filter values, creates an entry with a null value if a key is
     * not yet in the map
     */
    private final Map<String, Object> filters = new HashMap<>(3) {
        @Override
        public Object get(Object key) {
            if (!containsKey(key)) {
                put(String.valueOf(key), null);
            }
            return super.get(key);
        }

    };

    /**
     * Calls {@link CrudReadService#find(java.io.Serializable, java.lang.Class)
     * } with Integer.valueOf(rowKey)
     *
     * @param rowKey
     * @return
     */
    @Override
    public E getRowData(String rowKey) {
        return "".equals(rowKey) ? null : getCrudReadService().find(Integer.valueOf(rowKey), clazz);
    }

    /**
     * creates a {@link Builder }
     * using {@link AbstractLazyController#isSyntaxInValue() },
     * {@link Builder#DEFAULT_MAPPING} and {@link #isCaseInsensitive()}.
     *
     * @return
     */
    protected Param.Builder initParamBuilder() {
        return new Param.Builder(isSyntaxInValue(), Param.Builder.DEFAULT_MAPPING, isCaseInsensitive());
    }

    private final List<String> sortkols = new ArrayList<>(3);

    /**
     * Call {@link #initParamBuilder() } and call
     * {@link #addToParamBuilder(Builder, String, Object)} for each entry.
     * Filters holding empty or null value are skipped.
     *
     * @param filters
     * @return
     */
    protected List<Param> convertFilters(Map<String, FilterMeta> filters) {
        if (filters == null) {
            return null;
        }
        Param.Builder builder = initParamBuilder();
        for (Map.Entry<String, FilterMeta> p : filters.entrySet()) {
            Object fv = p.getValue().getFilterValue();
            if (!"".equals(fv) && fv != null) {
                addToParamBuilder(builder, p.getValue().getField(), fv);
            }
        }
        return builder.build();
    }

    /**
     * Implement this method to call one of the add methods in {@link Builder}.
     * If you want to benefit from syntax intelligence for non String values,
     * choose one of the add methods using a converter and use
     * String.valueOf(value) as paramValue.
     *
     * @see StringConverter#forClass(Class)
     * @see #isUseOr()
     * @param builder
     * @param key
     * @param value
     */
    protected abstract void addToParamBuilder(Param.Builder builder, String key, Object value);

    /**
     * Calls {@link #convertSortMeta(java.util.List) }, {@link #convertFilters(java.util.Map) },
     * {@link CrudReadService#findDynamic(java.lang.Integer, java.lang.Integer, java.util.Map, java.util.List, java.lang.Class) },
     * {@link #setWrappedData(java.lang.Object) } and {@link #setRowCount(int) }
     * with
     * {@link CrudReadService#countDynamic(java.util.List, java.lang.Class) }.
     *
     * @param first
     * @param pageSize
     * @param sortBy
     * @param filterBy
     * @return
     */
    @Override
    public List<E> load(int first, int pageSize, Map<String, SortMeta> sortBy, Map<String, FilterMeta> filterBy) {
        SORTORDER.Builder sortBuilder = sortBy!=null ? convertSortMeta(new ArrayList<>(sortBy.values())) : new SORTORDER.Builder();
        List<E> load = getCrudReadService().findDynamic(first, pageSize,
                sortBuilder.build(), convertFilters(filterBy), clazz);
        setWrappedData(load);
        return load;
    }

    @Override
    public int count(Map<String, FilterMeta> filters) {
        return getCrudReadService().countDynamic(convertFilters(filters), clazz);
    }
    

    /**
     * Convert primefaces SortMeta list into builder for use in CrudReadService,
     * retains the order in which users add columns to sort on when
     * sortMode="multiple"
     *
     * @see #convert(org.primefaces.model.SortOrder)
     * @param sortMetas
     * @return
     */
    protected SORTORDER.Builder convertSortMeta(List<SortMeta> sortMetas) {
        SORTORDER.Builder sortBuilder = new SORTORDER.Builder();
        if (sortMetas != null && !sortMetas.isEmpty()) {
            if (sortkols.isEmpty()) {
                // nu is de volgorde nog goed
                sortMetas.stream().filter(sm -> sm.getColumnKey() != null).forEach(sm -> sortkols.add(sm.getColumnKey()));
            } else {
                // voeg nieuwe kolommen toe in de juiste volgorde
                sortMetas.stream().filter(sm -> sm.getColumnKey() != null && !sortkols.contains(sm.getColumnKey()))
                        .forEach(sm -> sortkols.add(sm.getColumnKey()));
                sortkols.removeIf(k -> sortMetas.stream().noneMatch(sm -> k.equals(sm.getColumnKey())));
            }
            if (sortkols.isEmpty()) {
                SortMeta sortMeta = sortMetas.get(0);
                sortBuilder.add(sortMeta.getField(), convert(sortMeta.getOrder()));
            } else {
                // hanteer altijd de juiste volgorde
                sortkols.stream().map(k
                        -> sortMetas.stream().filter(sm -> k.equals(sm.getColumnKey())).findFirst().get())
                        .forEach((h) -> sortBuilder.add(h.getField(), convert(h.getOrder())));
            }
        }
        return sortBuilder;
    }

    public static SORTORDER convert(SortOrder order) {
        if (order == null) {
            return null;
        }
        return switch (order) {
            case ASCENDING -> SORTORDER.ASC;
            case UNSORTED -> SORTORDER.NONE;
            case DESCENDING -> SORTORDER.DESC;
            default -> throw new IllegalArgumentException("Cannot convert sort order: " + order);
        };
    }

    /**
     * Use this in primefaces EL expression for filteredValue
     *
     * @return
     */
    @Override
    public List<E> getFiltered() {
        return getWrappedData();
    }

    @Override
    public void setFiltered(List<E> filtered) {
    }

    /**
     * used this in EL expression for filterValue:
     * #{controller.filters['filtername']}
     *
     * @return
     */
    @Override
    public Map<String, Object> getFilters() {
        return filters;
    }

    @Override
    public Filtering<E> add(String key, Object value) {
        filters.put(key, value);
        return this;
    }

    @Override
    public Filtering<E> clear() {
        filters.clear();
        return this;
    }

    @Override
    public boolean hasFilter(String key) {
        return filters.containsKey(key) && filters.get(key) != null && !String.valueOf(filters.get(key)).isEmpty();
    }

    public void setSyntaxInValue(boolean syntaxInValue) {
        this.syntaxInValue = syntaxInValue;
    }

    /**
     * when true use or when several parameters are given
     *
     * @see Param#getAndOr()
     * @return
     */
    public boolean isUseOr() {
        return useOr;
    }

    public void setUseOr(boolean useOr) {
        this.useOr = useOr;
    }

    /**
     * when true support syntax in parameter value
     *
     * @see Builder
     * @return
     */
    public boolean isSyntaxInValue() {
        return syntaxInValue;
    }

    public boolean isCaseInsensitive() {
        return caseInsensitive;
    }

    public void setCaseInsensitive(boolean caseInsensitive) {
        this.caseInsensitive = caseInsensitive;
    }

    /**
     * Wraps a String value in * so a contains search is performed, calls
     * {@link Builder#add(String, Object, boolean)} with {@link #isUseOr() },
     * calls {@link Util#wrapStringInWildCards(String, boolean)} to do the actual wrapping.
     * For a quoted value an equals comparison will be added to the builder with the quotes stripped.
     *
     *
     * @see Util#wrapStringInWildCards(String, boolean)
     * @param builder
     * @param key
     * @param value
     */
    protected void wrapStringInWildCards(Param.Builder builder, String key, Object value) {
        if (value instanceof String) {
            String v = String.valueOf(value);
            if (v.startsWith("\"") && v.endsWith("\"")) {
                builder.add(key,key, OPERATOR.EQ,v.substring(1,v.length()-1),false,isUseOr());
            } else {
                builder.add(key,Util.wrapStringInWildCards(v,isSyntaxInValue()),isUseOr());
            }
        } else {
            builder.add(key, value, isUseOr());
        }

    }

    /**
     * This method makes your lazy datatables work also if you don't define a
     * rowkey attribute and function
     *
     * @param object
     * @return
     */
    @Override
    public String getRowKey(E object) {
        return String.valueOf(object.getId() == null ? -1 : object.getId());
    }

    @Inject
    private transient SecurityContext securityContext;

    @Serial
    private void readObject(java.io.ObjectInputStream stream) throws ClassNotFoundException, IOException {
        stream.defaultReadObject();
        securityContext = CDI.current().select(SecurityContext.class).get();
    }

    protected SecurityContext getSecurityContext() {
        return securityContext;
    }
}
