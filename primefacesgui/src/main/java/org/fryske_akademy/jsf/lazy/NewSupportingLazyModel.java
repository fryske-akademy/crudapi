/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.jsf.lazy;

/*-
 * #%L
 * primfacesgui
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jsf.AbstractLazyController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * LazyModel that supports creating new entities in a datatable.
 *
 * @author eduard
 */
public abstract class NewSupportingLazyModel<E extends EntityInterface> extends AbstractLazyModel<E> {

    /**
     * adds {@link AbstractLazyController#getNewEntity() new entity} to wrapped data if it is not null,
     * so table shows it, and you are able to change and store
     *
     * @param list
     */
    @Override
    public void setWrappedData(Object list) {
        if (getLazyController()!=null) {
            EntityInterface newEntity = getLazyController().getNewEntity();
            if (newEntity != null) {
                List wrappedData = new ArrayList((List) list);
                if (!wrappedData.contains(newEntity)) {
                    wrappedData.add(newEntity);
                    super.setWrappedData(Collections.unmodifiableList(wrappedData));
                    return;
                }
            }
        }
        super.setWrappedData(list);
    }

    /**
     * adds one to the number of rows when {@link AbstractLazyController#getNewEntity() } is not null.
     * @param rowCount 
     */
    @Override
    public void setRowCount(int rowCount) {
        super.setRowCount(getLazyController()==null||getLazyController().getNewEntity() == null ? rowCount : rowCount + 1);
    }

}
