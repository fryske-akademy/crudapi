/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.jsf;

/*-
 * #%L
 * primfacesgui
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.EntityInterface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class generic filtering support
 *
 * @author eduard
 */
public class FilteringImpl<E extends EntityInterface> implements Filtering<E> {

    /**
     * map holding filter values, creates an entry with a null value if a key is
     * not yet in the map
     */
    private final Map<String, Object> filters = new HashMap<>(3) {
        @Override
        public Object get(Object key) {
            if (!containsKey(key)) {
                put(String.valueOf(key), null);
            }
            return super.get(key);
        }

    };

    private List<E> filtered;

    /**
     * Use this in primefaces EL expression for filteredValue
     *
     * @return
     */
    @Override
    public List<E> getFiltered() {
        return filtered;
    }

    @Override
    public void setFiltered(List<E> filtered) {
        this.filtered = filtered;
    }

    /**
     * used this in EL expression for filterValue:
     * #{controller.filters['filtername']}
     *
     * @return
     */
    @Override
    public Map<String, Object> getFilters() {
        return filters;
    }

    @Override
    public Filtering<E> clear() {
        filters.clear();
        return this;
    }

    @Override
    public Filtering<E> add(String key, Object value) {
        filters.put(key, value);
        return this;
    }

    @Override
    public boolean hasFilter(String key) {
        return filters.containsKey(key)&&!Util.nullOrEmpty(String.valueOf(filters.get(key)));
    }

}
