package org.fryske_akademy.jsf.lazy;

/*-
 * #%L
 * primefacesgui
 * %%
 * Copyright (C) 2018 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.jsf.util.JsfUtil;
import org.fryske_akademy.services.CrudReadService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AbstractLazyModelTest {

    private AbstractLazyModel abstractLazyModel = new AbstractLazyModel() {


        @Override
        public CrudReadService getCrudReadService() {
            return null;
        }

        @Override
        protected void addToParamBuilder(Param.Builder builder, String key, Object value) {

        }
    };

    @Test
    public void testWrapStringInWildCards() {
        assertEquals("!*a*", Util.wrapStringInWildCards("!a", abstractLazyModel.isSyntaxInValue()));
        assertEquals(">a", Util.wrapStringInWildCards(">a", abstractLazyModel.isSyntaxInValue()));
        assertEquals("!>a", Util.wrapStringInWildCards("!>a", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*a*", Util.wrapStringInWildCards("a", abstractLazyModel.isSyntaxInValue()));
        assertEquals("d <=> g", Util.wrapStringInWildCards("d <=> g", abstractLazyModel.isSyntaxInValue()));
        assertEquals("!d <=> g", Util.wrapStringInWildCards("!d <=> g", abstractLazyModel.isSyntaxInValue()));
        assertEquals("is blank", Util.wrapStringInWildCards("is blank", abstractLazyModel.isSyntaxInValue()));
        assertEquals("is null", Util.wrapStringInWildCards("is null", abstractLazyModel.isSyntaxInValue()));
        assertEquals("is empty", Util.wrapStringInWildCards("is empty", abstractLazyModel.isSyntaxInValue()));
        assertEquals("!is blank", Util.wrapStringInWildCards("!is blank", abstractLazyModel.isSyntaxInValue()));
        assertEquals("!is null", Util.wrapStringInWildCards("!is null", abstractLazyModel.isSyntaxInValue()));
        assertEquals("!is empty", Util.wrapStringInWildCards("!is empty", abstractLazyModel.isSyntaxInValue()));
        abstractLazyModel.setSyntaxInValue(false);
        assertEquals("*>a*", Util.wrapStringInWildCards(">a", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*!>a*", Util.wrapStringInWildCards("!>a", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*a*", Util.wrapStringInWildCards("a", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*d <=> g*", Util.wrapStringInWildCards("d <=> g", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*!d <=> g*", Util.wrapStringInWildCards("!d <=> g", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*is blank*", Util.wrapStringInWildCards("is blank", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*is null*", Util.wrapStringInWildCards("is null", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*is empty*", Util.wrapStringInWildCards("is empty", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*!is blank*", Util.wrapStringInWildCards("!is blank", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*!is null*", Util.wrapStringInWildCards("!is null", abstractLazyModel.isSyntaxInValue()));
        assertEquals("*!is empty*", Util.wrapStringInWildCards("!is empty", abstractLazyModel.isSyntaxInValue()));
    }
}
